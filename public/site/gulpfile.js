'use strict';

var concat, gulp, uglify, templateCache;

gulp = require('gulp');

concat = require('gulp-concat');

uglify = require('gulp-uglify');

templateCache = require('gulp-angular-templatecache');

gulp.task('templates', function () {
    return gulp
        .src([
            'modules/**/*.html',
            'directives/**/*.html',
            'views/**/*.html'
        ])
        .pipe(templateCache(
            'templates.js',
            {
                base: __dirname,
                standalone: true
            }
        ))
        .pipe(gulp.dest('public/js'));
});

gulp.task('vendors', function() {
    return gulp.src([
        'bower_components/html5-boilerplate/js/vendor/modernizr-2.6.2.min.js',
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/jquery-bridget/jquery.bridget.js',
        'bower_components/jquery-modal/jquery.modal.js',
        'bower_components/masonry/dist/masonry.pkgd.min.js',
        'bower_components/imagesloaded/imagesloaded.pkgd.min.js',
        'bower_components/angular/angular.min.js',
        'bower_components/angular-masonry/angular-masonry.js',
        'bower_components/angular-route/angular-route.js',
        'bower_components/angular-resource/angular-resource.min.js',
        'bower_components/angular-ui-router/release/angular-ui-router.min.js',
        'bower_components/angular-sanitize/angular-sanitize.min.js',
        'bower_components/angular-hyper-resource/angular-hyper-resource.js',
        'bower_components/ngmap/app/scripts/app.js',
        'bower_components/ngmap/app/scripts/directives/map_controller.js',
        'bower_components/ngmap/app/scripts/directives/map.js',
        'bower_components/ngmap/app/scripts/directives/marker.js',
        'bower_components/ngmap/app/scripts/directives/shape.js',
        'bower_components/ngmap/app/scripts/services/geo_coder.js',
        'bower_components/ngmap/app/scripts/services/navigator_geolocation.js',
        'bower_components/ngmap/app/scripts/services/attr2_options.js',

        'static/js/modernizr.custom.grid.js',
        'static/js/jcarousel.responsive.js',
        'static/js/jquery.jcarousel.min.js',
        'static/js/jquery.slides.min.js',
        'static/js/jquery.pickmeup.js',
        'static/js/cbpTooltipMenu.min.js',
        'static/js/jquery-slidescrollpanel.js',
        'static/js/bootstrap.js',
        'static/js/slick.js'
    ])
        .pipe(concat('vendors.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js'));
});

gulp.task('scripts', function() {
    return gulp.src([
        'app.js',
        'config/*.js',
        'router/states.js',
        'helper/angular-loadscript.js',
        'directives/**/*.js',
        'dataSources/**/*.js',
        'controllers/*.js',
        'modules/**/*.js',
        'components/version/version.js',
        'components/version/version-directive.js',
        'components/version/interpolate-filter.js',
        'views/**/*.js',
        'public/js/templates.js'
    ])
    .pipe(concat('application.min.js'))
    //.pipe(uglify())
    .pipe(gulp.dest('public/js'));
});

gulp.task('default', ['templates', 'vendors', 'scripts']);

gulp.watch([
    'app.js',
    'router/states.js',
    'directives/**',
    'controllers/**',
    'modules/**',
    'views/**/*.js',

    'modules/**/*.html',
    'directives/**/*.html',
    'views/**/*.html'
], ['scripts', 'templates']);