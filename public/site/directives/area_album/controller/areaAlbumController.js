app.controller('areaAlbumController', ['generalInfo', 'landmarksHal', '$stateParams', 'mainConfig',
    function (generalInfo, landmarksHal, $stateParams, mainConfig) {

        var that         = this,
            countryId    = $stateParams.countryAlias;
        
        if($stateParams.generalType === undefined){
            that.generalType  = 'main';
        }else{
            that.generalType  = $stateParams.generalType;    
        }
        
        that.baseUrl = mainConfig.basePublicUrl;
        that.vert = false;
        
        generalInfo.query(
            {
                area_id  : countryId,
                fields   : 'image.files,gallery',
                filter	 : 'gallery.status:eq:1'
            },
            function(data) {
                that.area = data;
            }
        )
        .$promise.then(function(){
            if(that.area.image.name != 'System'){
                for(var i =0; i < that.area.image.files.length; i++){
                    if(parseInt(that.area.image.files[i].size) == 4){
                        that.mainFoto = that.area.image.files[i];
                        if(parseInt(that.mainFoto.height) > parseInt(that.mainFoto.width)){
                    	    that.vert = true;
                    	}
                    }
                }
            }
            
            var photoCount = that.area.gallery.length;
            for (var curPhoto = 0; curPhoto < photoCount; curPhoto++) {
                var curFilesLength = that.area.gallery[curPhoto].files.length;

                for (var i = 0; i < curFilesLength; i++) {
            	    var curFile = curFilesLength - i - 1;
                    
                	if (parseInt(that.area.gallery[curPhoto].files[curFile]['size']) == 4) {
            			if(curPhoto==0){
            			    that.foto1 = that.area.gallery[curPhoto].files[curFile];
            			     if(parseInt(that.foto1.height) > parseInt(that.foto1.width)){
            			        that.vert = true;
            			     }
            			}else if(curPhoto==1){
            			    that.foto2 = that.area.gallery[curPhoto].files[curFile];
            			    if(parseInt(that.foto2.height) > parseInt(that.foto2.width)){
            			        that.vert = true;
            			    }
            			}
            		}
            	}
            } 
        
            if(that.area.gallery.length == 0 || that.mainFoto === undefined || that.foto2 === undefined){
                var filterL = [
                                'area:eq:' + countryId,
                                'image.id:notin:10,11,12,13,14'
                              ];
                filterL = filterL.join(mainConfig.filterSeparator);
                landmarksHal.query(
                    {
                        filter      : filterL,
                        fields      : 'image',
                        pagesize    :  3,
                        order       : 'rating:desc'
                    },
                    function(data){
                        that.landmarks = data;
                    }
                )
                .$promise.then(function(){
                    var landmarkCount = that.landmarks.results.length;
                    for (var curPhoto = 0; curPhoto < landmarkCount; curPhoto++) {
                        var curFilesLength = that.landmarks.results[curPhoto].image.files.length;

                        for (var i = 0; i < curFilesLength; i++) {
                    	    var curFile = curFilesLength - i - 1;
                    	    if(parseInt(that.landmarks.results[curPhoto].image.files[curFile]['size']) == 4 && that.mainFoto === undefined){
                    	        that.mainFoto = that.landmarks.results[curPhoto].image.files[curFile];
                    	        if(parseInt(that.mainFoto.height) > parseInt(that.mainFoto.width)){
                    	            that.vert = true;
                    	        }
                    	    }else if (parseInt(that.landmarks.results[curPhoto].image.files[curFile]['size']) == 4 && that.foto1 === undefined) {
            			        that.foto1 = that.landmarks.results[curPhoto].image.files[curFile];
            			        if(parseInt(that.foto1.height) > parseInt(that.foto1.width)){
            			            that.vert = true;
            			        }
            		        }else if (parseInt(that.landmarks.results[curPhoto].image.files[curFile]['size']) == 4 && that.foto2 === undefined){
            		            that.foto2 = that.landmarks.results[curPhoto].image.files[curFile];
            		            if(parseInt(that.foto2.height) > parseInt(that.foto2.width)){
            			            that.vert = true;
            			        }
            		        }
            	        }
                    } 
                });
            }
            
            
        });
        
        
    }
]);