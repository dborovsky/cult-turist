app.directive('imgOnLoad',['$stateParams', function ($stateParams) {       
    return {
        link: function($scope, element, attrs, $stateParams) {   

            element.bind("load" , function(e){
                    $('.one-time').slick({
                        dots: false,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 1,
                        adaptiveHeight: true,
                        asNavFor: '.slider-nav'
                    });
                    $('.slider-nav').slick({
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        asNavFor: '.one-time',
                        adaptiveHeight: true,
                        dots: false,
                        infinite: true,
                        centerMode: true,
                        focusOnSelect: true,
                        arrows: false
                    });
                    if(this.className == 'land'){
                            $('.one-time').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	                            var item_length = slick.$slides.length - 3;
	                            if( item_length == nextSlide ){
                                    angular.element('#galleryFoto').scope().galCtrl.loadNext();
	                            }
	        
                            });        
                    }
                        
            });

        }
    }
}]);
//countryLandmarks
app.controller('galleryController', ['generalInfo', 'landmarkInfo', 'landmarksHal', 'areaEntityList', 'entityDirectList', '$stateParams', 'mainConfig', 'constants', '$scope', '$location',

    function(generalInfo, landmarkInfo, landmarksHal, areaEntityList, entityDirectList, $stateParams, mainConfig, constants, $scope, $location){
        var that = this,
            countryAlias    = $stateParams.countryAlias,
            entitesNames = ['landmark', 'people', 'feature', 'kitchen'],
            fieldsArray     = [
                'web_image',
            ],
            lmFieldsArray     = [
                'image.files'
            ],
            pageNumber      = 1,
            fields,
            queryFilter = [
                'area:eq:' + countryAlias,
                'status:eq:1',
                'area.system:eq:0'
            ],
            listSize          = 5,
            lmFields          = lmFieldsArray.join(),
            filter = queryFilter.join(mainConfig.filterSeparator);
        that.entityItemId    = $stateParams.landmarkId,
        that.baseUrl = mainConfig.basePublicUrl;
        that.formedGallery = [];
        fields = fieldsArray.join();
        
        /*полная галерея гео-объекта*/
        if($stateParams.landmarkId === undefined){
        function shuffle(array) {
            var currentIndex = array.length, temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        }
        
        /*full query*/
        
        var setEntityQuery = function(entityName) {
            
            var filter = [
                'area:eq:' + countryAlias,
                'area.system:eq:0',
                'web_image.id:notin:10,11,12,13,14'
            ];

            filter = filter.join(mainConfig.filterSeparator);
            
            return {
                entity  : entityName,
                page    : pageNumber,
                filter  : filter,
                fields  : fields,
                pagesize: listSize
            }
        };

        var setLandmarkQuery = function() {
            var filter = [
                'area:eq:' + countryAlias,
                'image.id:not:10'
            ];

            filter = filter.join(mainConfig.filterSeparator);

            return {
                page        : pageNumber,
                filter      : filter,
                fields      : lmFields,
                pagesize    : listSize,
                order       : 'rating:desc'
            }
        };
        
        var dummy = [];
        generalInfo.query(
            {
                area_id  : countryAlias,
                fields   : 'image.files,gallery',
                filter	 : 'gallery.status:eq:1'
            },
            function(data) {
                dummy['area'] = data;
            }
        )
        .$promise.then(function(){
        landmarksHal.query(
                setLandmarkQuery(),
                function (data) {
                    dummy[entitesNames[0]] = data.results;
                })
                .$promise.then(function () {
                    // теперь люди, фишки, кухни
                    var entityName = entitesNames[1];
                    entityDirectList.query(
                        setEntityQuery(entityName),
                        function (data) {
                            dummy[entityName] = data.results;
                        })
                        .$promise.then(function () {
                            var entityName = entitesNames[2];
                            entityDirectList.query(
                                setEntityQuery(entityName),
                                function (data) {
                                    dummy[entityName] = data.results;
                                })
                                .$promise.then(function () {
                                    var entityName = entitesNames[3];
                                    entityDirectList.query(
                                        setEntityQuery(entityName),
                                        function (data) {
                                            dummy[entityName] = data.results;
                                        })
                                        .$promise.then(function () {
                                            //console.log(dummy);
                                            //that.mainImageFileIndex = {};
                                            for(var entN = 3; entN >= 0; entN--) {
                                                for (var i = 0; i < dummy[entitesNames[entN]].length; i++) {
                                                    if(entitesNames[entN] == 'landmark'){
                                                        for (var j = 0; j < dummy[entitesNames[entN]][i].image.files.length; j++) {
                    	                                    if (dummy[entitesNames[entN]][i].image.files[j]['path'].indexOf('original') != -1 && dummy[entitesNames[entN]][i].image.files[j]['path'].indexOf('placeholder') == -1) {
                    		                                    if(dummy[entitesNames[entN]][i].rating >= 3){
                    		                                        that.formedGallery.unshift(dummy[entitesNames[entN]][i].image.files[j]);
                    		                                    }else{
                    		                                        that.formedGallery.push(dummy[entitesNames[entN]][i].image.files[j]);
                    		                                    }
                    	                                    }
                                                        }
                                                    }else{
                                                        for (var j = 0; j < dummy[entitesNames[entN]][i].web_image.files.length; j++) {
                    	                                    if (dummy[entitesNames[entN]][i].web_image.files[j]['path'].indexOf('original') != -1 && dummy[entitesNames[entN]][i].web_image.files[j]['path'].indexOf('placeholder') == -1) {
                    		                                    that.formedGallery.push(dummy[entitesNames[entN]][i].web_image.files[j]);
                    	                                    }
                                                        }
                                                        that.formedGallery = shuffle(that.formedGallery);
                                                    }
                                                    
                                                }
                                            }
                                            
                                            for (var j = 0; j < dummy['area'].image.files.length; j++) {
                    	                                    if (dummy['area'].image.files[j]['path'].indexOf('original') != -1 && dummy['area'].image.files[j]['path'].indexOf('placeholder') == -1) {
                    		                                    that.formedGallery.unshift(dummy['area'].image.files[j]);
                    	                                    }
                                                        }
                                            for (var j = 0; j < dummy['area'].gallery.length; j++) {
                                                for(var galI = 0; galI < dummy['area'].gallery[j].files.length; galI++){
                    	                            if (dummy['area'].gallery[j].files[galI].path.indexOf('original') != -1) {
                    		                            that.formedGallery.unshift(dummy['area'].gallery[j].files[galI]);      
                    	                            }
                                                }
                                            }
                                            
                                        });
                                });
                        });
                });
        });
        that.loadNext = function(){
            var tempFoto = [];
            pageNumber++;
            landmarksHal.query(
                setLandmarkQuery(),
                function (data) {
                    dummy[entitesNames[0]] = data.results;
                })
                .$promise.then(function () {
                    // теперь люди, фишки, кухни
                    var entityName = entitesNames[1];
                    entityDirectList.query(
                        setEntityQuery(entityName),
                        function (data) {
                            dummy[entityName] = data.results;
                        })
                        .$promise.then(function () {
                            var entityName = entitesNames[2];
                            entityDirectList.query(
                                setEntityQuery(entityName),
                                function (data) {
                                    dummy[entityName] = data.results;
                                })
                                .$promise.then(function () {
                                    var entityName = entitesNames[3];
                                    entityDirectList.query(
                                        setEntityQuery(entityName),
                                        function (data) {
                                            dummy[entityName] = data.results;
                                        })
                                        .$promise.then(function () {
                                            for(var entN = 3; entN >= 0; entN--) {
                                                for (var i = 0; i < dummy[entitesNames[entN]].length; i++) {
                                                    if(entitesNames[entN] == 'landmark'){
                                                        for (var j = 0; j < dummy[entitesNames[entN]][i].image.files.length; j++) {
                    	                                    if (dummy[entitesNames[entN]][i].image.files[j]['path'].indexOf('original') != -1 && dummy[entitesNames[entN]][i].image.files[j]['path'].indexOf('placeholder') == -1) {
                    		                                    if(dummy[entitesNames[entN]][i].rating >= 3){
                    		                                        tempFoto.unshift(dummy[entitesNames[entN]][i].image.files[j]);
                    		                                    }else{
                    		                                        tempFoto.push(dummy[entitesNames[entN]][i].image.files[j]);
                    		                                    }
                    	                                    }
                                                        }
                                                    }else{
                                                        for (var j = 0; j < dummy[entitesNames[entN]][i].web_image.files.length; j++) {
                    	                                    if (dummy[entitesNames[entN]][i].web_image.files[j]['path'].indexOf('original') != -1 && dummy[entitesNames[entN]][i].web_image.files[j]['path'].indexOf('placeholder') == -1) {
                    		                                    tempFoto.push(dummy[entitesNames[entN]][i].web_image.files[j]);
                    	                                    }
                                                        }
                                                        tempFoto = shuffle(tempFoto);
                                                    }
                                                    
                                                }
                                            }
                                            
                                            
                                            
                                            for(i= 0; i< tempFoto.length; i++){
                                                //that.formedGallery.push(tempFoto[i]);
                                                
                                                $('.one-time').slick('slickAdd','<div><img src="'+tempFoto[i].path+'"></div>');
                                                $('.slider-nav').slick('slickAdd','<div><img src="'+tempFoto[i].path+'"></div>');
                                            }
                                            
                                        });
                                });
                        });
                });
        };
        
        
        }
        
        
        /*запрос единственной достопримечательности*/
        if($stateParams.landmarkId != undefined){
            var entityName = 'landmark';
        landmarkInfo.query(
            {
                entityName  : entityName,
                landmarkId  : that.entityItemId,
                fields      : 'image,gallery',
                filter		: 'gallery.status:eq:1'
            },
            function(data) {
                that.entityInfo = data;
                
                    for (var i = 0; i < data.image.files.length; i++) {
                    	if (data.image.files[i]['path'].indexOf('original') != -1) {
                    		that.formedGallery.push(data.image.files[i]);
                    	}
                    }

                    
                    var photoCount = data.gallery.length;
                    
                    for (var curPhoto = 0; curPhoto < photoCount; curPhoto++) {
                    	var curFilesLength = data.gallery[curPhoto].files.length;

                    	for (var i = 0; i < curFilesLength; i++) {
                    		var curFile = curFilesLength - i - 1;

                    		if (data.gallery[curPhoto].files[curFile]['path'].indexOf('original') != -1) {
                    			
                        			that.formedGallery.push(data.gallery[curPhoto].files[curFile]);
                        			
                    		}
                    	}
                    }
            }
        );
    }
        
    }]
);