app.controller('EntityBindingController', ['generalInfo', 'entityInfo','$stateParams',
    function (generalInfo, entityInfo,$stateParams) {

        var that         = this,
            filter       = '',
            entity,
            entityItemId = $stateParams.entityItemId,
            countryId = $stateParams.countryAlias,
            entityName 		= $stateParams.entityName,
            fieldsArray     = ['area','area.upper','area.upper.name'],
            fields;
            
            

        //в будущем лучше использовать наследование контроллеров
        if (typeof $stateParams.generalType != 'undefined') {
            entity       = 'area';
            //entityItemId = $stateParams.countryAlias;
        }
        else if (typeof $stateParams.landmarkId != 'undefined') {
            entity       = 'landmark';
            //entityItemId = $stateParams.landmarkId;
        }
        else {
            entity       = $stateParams.entityName;
            //entityItemId = $stateParams.entityItemId;
        }


        if (entity != 'landmark')
        {
            filter = 'area.system:eq:0';
        }

        generalInfo.query(
            {
                area_id: countryId,
                fields   : 'upper',
                filter   : filter
            },
            function(data) {
                that.bind_areas = data.upper;

                var current_area = {
                    id        : data.id,
                    area_type : data.area_type,
                    name      : data.name
                };

                that.bind_areas.push(current_area);
            }
        ).$promise.then(function (){
            if(entity != 'landmark'){
                fields = fieldsArray.join();
                entityInfo.query(
                    {
                        entity_name : entityName,
                        entity_id   : entityItemId,
                        fields      : fields,
                        filter      : 'area.system:eq:0'
                    },
                    function(data) {
                        that.allBinds = data;
                    }
                );
            }
        });
    }
]);

