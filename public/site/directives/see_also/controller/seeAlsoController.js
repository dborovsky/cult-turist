app.controller('seeAlsoController', ['seeAlsoInfo', '$stateParams', 'mainConfig', '$scope',
    function (seeAlsoInfo, $stateParams, mainConfig, $scope) {
        var that            = this,
            entityName      = $stateParams.entityName,
            entityItemId    = $stateParams.entityItemId,
            //landmarkId    = $stateParams.landmarkId,
            fieldsArray     = [
                'name',
                'area'
                ],
            fields;

        that.baseUrl = mainConfig.basePublicUrl;

        if(typeof $stateParams.landmarkId != 'undefined'){
            entityName = 'landmark';
        }
        if (entityName === 'landmark'){
            fieldsArray.push('property.icon.files');
            entityItemId = $stateParams.landmarkId;
        } else
        {
        	fieldsArray.push('web_image.files');
        }

        fields = fieldsArray.join();

        seeAlsoInfo.query(
            {
                entity_name : entityName,
                entity_id   : entityItemId,
                fields      : fields
            },
            function(data) {
            	that.entityName = entityName;
                that.seeAlso = data.data;
            }
        );
    }
]);