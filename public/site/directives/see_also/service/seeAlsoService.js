/**
 *
 * @param $resource
 * @param mainConfig
 * @returns {*}
 */

app.factory("seeAlsoInfo", ['$resource', 'mainConfig',
    function ($resource, mainConfig) {
        return $resource(
            mainConfig.apiUrl + 'seealso',
            {
                entity_name : ':entity_name',
                entity_id: ':entity_id',
                fields   : ':fields'
            },
            {
                query: {
                    method: "GET"
                }
            }
        );
    }
]);