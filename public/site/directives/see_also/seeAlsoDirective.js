app.directive('seeAlso', function() {
    return {
        scope: {anEntity:'='},
        templateUrl: 'directives/see_also/view/seeAlso.html'
    };
});