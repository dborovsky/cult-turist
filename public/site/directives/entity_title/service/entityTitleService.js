/**
 *
 * @param $resource
 * @param mainConfig
 * @returns {*}
 */

app.factory("entityTitleInfo", ['$resource', 'mainConfig',
    function ($resource, mainConfig) {
        return $resource(
            mainConfig.apiUrl + ':entity/:entity_id',
            {
                entity   : ':entity',
                entity_id: ':entity_id',
                fields   : ':fields'
            },
            {
                query: {
                    method: "GET"
                }
            }
        );
    }
]);