app.directive('entityTitle', function($http,$compile,$rootScope, $route) {
    return {
        //templateUrl: 'directives/entity_title/view/entityTitle.html',
        link: function(scope, element, attr,$el){
            //$route.reload();
            $rootScope.safeApply = function(fn) {
                var phase = this.$root.$$phase;
                if(phase == '$apply' || phase == '$digest') {
                    if(fn && (typeof(fn) === 'function')) {
                        fn();
                    }   
                } else {
                    this.$apply(fn);
                }
            };
            
            $http.get('directives/entity_title/view/entityTitle.html').then(
            function(html){
               //console.dir(html);
               
               //console.log(x);
               $rootScope.safeApply(function() {
                   var template = angular.element(html.data);
                    var x =$compile(template)(scope);
                    element.html(x);
               });
               
               //scope.apply();
            });
        }
    };
});
