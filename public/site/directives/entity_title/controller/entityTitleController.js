app.controller('EntityTitleController', ['entityTitleInfo', '$stateParams',
    function (entityTitleInfo, $stateParams) {

        var that         = this,
            showWasHere  = false,
            entityItemId,
            fieldsArray     = [
                'original_name'
            ];

        that.entity = $stateParams.entityName;
        that.showTitle = false;
        that.isLandmark = false;

        // в будущем лучше использовать наследование контроллеров
        if (typeof $stateParams.generalType != 'undefined') {
            that.entity       = 'area';
            entityItemId = $stateParams.countryAlias;
        }
        else if (typeof $stateParams.landmarkId != 'undefined') {
            that.entity       = 'landmark';
            entityItemId = $stateParams.landmarkId;
            showWasHere  = true;
            if (that.entity == "landmark")
                that.isLandmark = true;
        }
        else {
            that.entity       = $stateParams.entityName;
            entityItemId = $stateParams.entityItemId;
            /*if (entity == "landmark")
                that.isLandmark = true;*/
        }

        if (that.entity == 'event')
        {
            fieldsArray.push('date');
        }

        that.showWasHere = showWasHere;

        entityTitleInfo.query(
            {
                entity   : that.entity,
                entity_id: entityItemId,
                fields: fieldsArray
            },
            function(data) {
                that.entityInfo = data;
            }
        ).$promise.then(function(){
                that.showTitle = true;
            });
    }
]);