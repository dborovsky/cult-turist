app.directive('geoLinks', function() {
    return {
        restrict: 'EA',
        scope: {areas:'='},
        templateUrl: 'directives/geo_links/view/geoLinks.html'
    };
});