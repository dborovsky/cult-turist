app.controller('advertController', ['generalInfo', 'advertsInfo', '$stateParams', 'mainConfig', '$scope', '$sce', '$location', '$attrs', '$route',
    function (generalInfo, advertsInfo, $stateParams, mainConfig, $scope, $sce, $location, $attrs, $route) {
        //var position = angular.element;
        //$scope.$apply();
        $scope.topAdvert = false;
        $route.reload();
        var stateConditions = $location.path().split("/");
        $scope.sce = $sce;
        var that            = this,
            position        = $attrs.type,
            countryAlias    = stateConditions[2],
            fieldsArray     = [
                'name',
                'area',
                'banner',
                'banner_link',
                'widget'
            ],
            
            fields          = fieldsArray.join(),
            order           = 'id:desc',
            pagesize        = 1;
            
            if(position == 'top'){
                pagesize        = 1;
            }else if(position == 'left'){
                pagesize        = 3;
            }
            
            
        if(stateConditions[3] === undefined || stateConditions[3] === 'list'){
                that.state = 'area_flag';
        }else if(stateConditions[3] != undefined){
            that.state = stateConditions[3];
        }
        var queryFilter = [
                'area:eq:' + countryAlias,
                'status:eq:1',
                'position:eq:'+position,
                that.state+':eq:1'
            ],
            filter = queryFilter.join(mainConfig.filterSeparator);
        //console.dir(that.state);
        that.baseUrl            = mainConfig.basePublicUrl;
        
        generalInfo.query(
            {
                area_id: countryAlias,
                filter: '',
                fields: 'upper' // все регионы и города страны
            },
            function(data) {
                $scope.topAdvert = true;
                that.areaType = data.area_type;
                if(that.areaType > 0){
                    var i;
                    for(i = 0; i<data.upper.length; i++){
                        if(data.upper[i].area_type == 0){
                            that.countryID = data.upper[i].id;    
                        }
                    }
                        //that.countryID = data.upper[0].id;    
                }
                //that.generalInfo = data.upper;
            }
        ).$promise.then(function(){
            advertsInfo.query(
            {
                filter      : filter,
                fields      : fields,
                pagesize    : pagesize,
                page        : 1,
                order       : order
            },
            function (data) {
                that.adverts = data.results;
            }
         ).$promise.then(function(){
            if((that.areaType > 0 && that.adverts.length == 0) || (that.areaType > 0 && that.adverts.length < pagesize)){
               queryFilter = [
                    'area:eq:' + that.countryID,
                    'status:eq:1',
                    'position:eq:'+position,
                    that.state+':eq:1'
               ],
               filter = queryFilter.join(mainConfig.filterSeparator);
               advertsInfo.query(
               {
                    filter      : filter,
                    fields      : fields,
                    pagesize    : pagesize,
                    page        : 1,
                    order       : order
                },
                function (data) {
                    that.adverts      = data.results;
                }).$promise.then(function(){
                  if(that.adverts.length == 0 || that.adverts.length < pagesize) {
                      queryFilter = [
                        'for_all:eq:1',
                        'status:eq:1',
                        'position:eq:'+position,
                        that.state+':eq:1'
                      ],
                      filter = queryFilter.join(mainConfig.filterSeparator);
                      advertsInfo.query(
                      {
                        filter      : filter,
                        fields      : fields,
                        pagesize    : pagesize,
                        page        : 1,
                        order       : order
                    },
                    function (data) {
                        
                        that.adverts = that.adverts.concat(data.results);
                    }).$promise.then(function(){
                        var i =0;
                        if (that.adverts.length > pagesize){
                            that.adverts.splice(pagesize-1);
                        }
                        for(i = 0; i < that.adverts.length; i++){
                            if(that.adverts[i].banner != undefined){
                                if(that.adverts[i].banner.path.indexOf('swf') + 1){
                                    that.adverts[i].type = 'flash';
                                }else{
                                    that.adverts[i].type = 'image';
                                }
                            }else{
                                that.adverts[i].type = 'text';
                            }
                        }
                        that.adverts.push({type: 'empty'}); 
                    });
                    }else{
                      var i =0;
                        if (that.adverts.length > pagesize){
                            that.adverts.splice(pagesize-1);
                        }
                        for(i = 0; i < that.adverts.length; i++){
                            if(that.adverts[i].banner != undefined){
                                if(that.adverts[i].banner.path.indexOf('swf') + 1){
                                    that.adverts[i].type = 'flash';
                                }else{
                                    that.adverts[i].type = 'image';
                                }
                            }else{
                                that.adverts[i].type = 'text';
                            }
                        }
                        that.adverts.push({type: 'empty'});    
                    }
                });
            }else if(that.adverts.length < pagesize){
                queryFilter = [
                        'for_all:eq:1',
                        'status:eq:1',
                        'position:eq:'+position,
                        that.state+':eq:1'
                      ],
                      filter = queryFilter.join(mainConfig.filterSeparator);
                      advertsInfo.query(
                      {
                        filter      : filter,
                        fields      : fields,
                        pagesize    : pagesize,
                        page        : 1,
                        order       : order
                    },
                    function (data) {
                        
                        that.adverts = that.adverts.concat(data.results);
                    }).$promise.then(function(){
                        var i =0;
                        if (that.adverts.length > pagesize){
                            that.adverts.splice(pagesize-1);
                        }
                        for(i = 0; i < that.adverts.length; i++){
                            if(that.adverts[i].banner != undefined){
                                if(that.adverts[i].banner.path.indexOf('swf') + 1){
                                    that.adverts[i].type = 'flash';
                                }else{
                                    that.adverts[i].type = 'image';
                                }
                            }else{
                                that.adverts[i].type = 'text';
                            }
                        }
                        that.adverts.push({type: 'empty'}); 
                    });
            }else{
                var i =0;
                        if (that.adverts.length > pagesize){
                            that.adverts.splice(pagesize-1);
                        }
                        for(i = 0; i < that.adverts.length; i++){
                            if(that.adverts[i].banner != undefined){
                                if(that.adverts[i].banner.path.indexOf('swf') + 1){
                                    that.adverts[i].type = 'flash';
                                }else{
                                    that.adverts[i].type = 'image';
                                }
                            }else{
                                that.adverts[i].type = 'text';
                            }
                        }
                        that.adverts.push({type: 'empty'}); 
            }
           });
        });
    }
]);
app.directive('myRepeatDirective', function() {
  return function(scope, element, attrs) {
    //angular.element(element).css('color','blue');
    if (scope.$last){
      var $element = $('div[id^="flash"]');
      
      if($element.length > 0){
          var id, width, height, path, i;
          for(i =0; i < $element.length; i++){
          
            id     = $element[i].id;
            width  = $('#'+id).attr('data-width');
            height = $('#'+id).attr('data-height');
            path   = $('#'+id).attr('data-path');
          
            swfobject.embedSWF(path, id, width, height, "8.0.0");
            
          }
      }
    }
  };
})
.directive('myMainDirective', function() {
  return function($scope, element, attrs) {
    //angular.element(element).css('border','5px solid red');
  };
});