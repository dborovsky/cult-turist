/**
 *
 * @param $resource
 * @param mainConfig
 * @returns {*}
 */

app.factory("advertInfo", ['$resource', 'mainConfig',
    function ($resource, mainConfig) {
        return $resource(
            mainConfig.apiUrl + 'iblock/:advert_id',
            {
                advert_id: ':advert_id',
                fields   : ':fields'
            },
            {
                query: {
                    method: "GET"
                }
            }
        );
    }
]);