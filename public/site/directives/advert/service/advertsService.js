/**
 *
 * @param $resource
 * @param mainConfig
 * @returns {*}
 */

app.factory("advertsInfo", ['$resource', 'mainConfig',
    function ($resource, mainConfig) {
        return $resource(
            mainConfig.apiUrl + 'iblock',
            {
                filter:     ':filter',
                fields:     ':fields',
                pagesize:   ':pagesize',
                order:      ':order'
            },
            {
                query: {
                    method: "GET",
                    isArray: false,
                    headers: {'Accept': 'application/hal+json, text/plain, */*'}
                }
            }
        );
    }
]);