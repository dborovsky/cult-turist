/**
 *
 * @param $resource
 * @param mainConfig
 * @returns {*}
 */

app.factory("countryLandmarks", ['$resource', 'mainConfig',
    function ($resource, mainConfig) {
        return $resource(
            mainConfig.apiUrl + ':entity',
            {
                entity:     'landmark',
                filter:     ':filter',
                fields:     ':fields',
                pagesize:   ':pagesize',
                order:      ':order'
            },
            {
                query: {
                    method: "GET",
                    isArray: false
                }
            }
        );
    }
]);