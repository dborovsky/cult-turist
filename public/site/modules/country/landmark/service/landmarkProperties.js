/**
 *
 * @param $resource
 * @param mainConfig
 * @returns {*}
 */

app.factory("landmarkProperties", ['$resource', 'mainConfig',
    function ($resource, mainConfig){
        return $resource(
            mainConfig.apiUrl + ':entity/:property_id',
            {
                entity      : 'landmarkproperty',
                property_id : ':property_id',
                fields      : ':fields'
            },
            {
                query: {
                    method: "GET",
                    isArray: false
                }
            });
    }
]);