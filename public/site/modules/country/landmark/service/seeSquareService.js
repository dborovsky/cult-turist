/**
 *
 * @param $resource
 * @param mainConfig
 * @returns {*}
 */

app.factory("SquareInfo", ['$resource', 'mainConfig',
    function ($resource, mainConfig) {
        return $resource(
            mainConfig.apiUrl + 'seesquare',
            {
                entity_name : ':entity_name',
                entity_id   : ':entity_id',
                fields      : ':fields',
            },
            {
                query: {
                    method: "GET"
                }
            }
        );
    }
]);