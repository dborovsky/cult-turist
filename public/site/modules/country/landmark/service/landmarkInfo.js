/**
 *
 * @param $resource
 * @param mainConfig
 * @returns {*}
 */

app.factory("landmarkInfo", ['$resource', 'mainConfig',
    function ($resource, mainConfig) {
        return $resource(
            mainConfig.apiUrl + ':entityName/:landmarkId',
            {
                entityName : 'landmark',
                landmarkId : ':landmarkId', // значение параметра задаётся в контроллере
                fields     : ':fields'
            },
            {
                query: {
                    method: "GET"
                }
            }
        );
    }
]);