/**
 *
 * @param $hResource
 * @param mainConfig
 * @returns {*}
 */

app.service('landmarksHal', ['hResource', 'mainConfig',
    function(hResource, mainConfig) {
        return hResource(
            mainConfig.apiUrl + ':entity',
            {
                entity:     'landmark',
                filter:     ':filter',
                fields:     ':fields',
                pagesize:   ':pagesize',
                order:      ':order'
            },
            {
                query: {
                    method: "GET",
                    isArray: false,
                    headers: {'Accept': 'application/hal+json, text/plain, */*'}
                }
            }
        );
    }
]);