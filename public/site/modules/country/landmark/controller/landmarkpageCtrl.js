app.controller('landmarkpageCtrl', ['landmarkInfo', '$stateParams', 'mainConfig',
    function (landmarkInfo, $stateParams, mainConfig) {
        var that            = this,
            entityName 		= 'landmark',
            landmarkId      = $stateParams.landmarkId,
            fieldsArray     = [
                'text',
                'area',
                'detail',
                'image.files'
                ],
            fields;

        that.entityName = 'landmark';
        that.baseUrl = mainConfig.basePublicUrl;

        //if (entityName === 'landmark'){
            fieldsArray.push('full_text');
            fieldsArray.push('property.icon.files');
            fieldsArray.push('gallery');

            fields = fieldsArray.join();
            
            landmarkInfo.query(
                {
                    entity_name   : entityName,
                    landmarkId    : landmarkId,
                    fields        : fields,
                    filter		  : 'gallery.status:eq:1'
                },
                function(data) {
                    that.entityInfo = data;

                    var mainImageHeight = 0;
                    for (var i = 0; i < data.image.files.length; i++) {
                    	if (parseInt(data.image.files[i]['width']) === 460) {
                    		mainImageHeight = data.image.files[i]['height'];
                    		that.mainImageFileIndex = i;
                    	}
                    }

                    that.morePhotoFileIndex = {};
                    var photoCount = data.gallery.length;
                    var filledHeight = 0;
                    for (var curPhoto = 0; curPhoto < photoCount; curPhoto++) {
                    	var curFilesLength = data.gallery[curPhoto].files.length;

                    	for (var i = 0; i < curFilesLength; i++) {
                    		var curFile = curFilesLength - i - 1;

                    		if (parseInt(data.gallery[curPhoto].files[curFile]['width']) === 214) {
                    			var curHeight = data.gallery[curPhoto].files[curFile]['height'];

                    			if (mainImageHeight - filledHeight - curHeight < 40) {
                        			continue;
                        		} else {
                        			that.morePhotoFileIndex[curPhoto] = curFile;
                        			filledHeight += curHeight;
                        		}
                    		}
                    	}
                    }
                }
            );
        //} 
    }
]);