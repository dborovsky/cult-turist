    app.controller('seeSquareController', ['landmarkInfo','SquareInfo', '$stateParams', 'mainConfig', 'constants', '$scope', '$location',

    function (landmarkInfo,SquareInfo, $stateParams, mainConfig, constants, $scope, $location) {
        var that            = this,
            entityName 		= 'landmark',
            entityItemId    = $stateParams.landmarkId,
            fieldsArray     = [
                'text',
                'area',
                'detail',
                'image.files'
                ],
            fieldsArrayseeSquare     = [
                'name',
                'area',
                'image.files'
                ],
            fields;
       
        that.baseUrl = mainConfig.basePublicUrl;
        
        fieldsArrayseeSquare.push('property.icon.files');
            

        fields = fieldsArrayseeSquare.join();
        
            SquareInfo.query(
                {
                    entity_name : entityName,
                    entity_id   : entityItemId,
                    fields      : fields
                },
                function(data) {
            	    that.entityName = entityName;
                    that.seeSquare = data.data;
                    
                    fieldsArray.push('full_text');
                    fieldsArray.push('property.icon.files');
                    fieldsArray.push('gallery');

                    fields = fieldsArray.join();
            
                    landmarkInfo.query(
                        {
                            entityName : entityName,
                            landmarkId  : entityItemId,
                            fields      : fields,
                            filter		: 'gallery.status:eq:1'
                        },
                        function(data) {
                            that.entityInfo = data;
                            setMap(that.entityInfo, that.seeSquare, $scope.map);
                        }
                    );
                }
            );
            
            var parseCoordinates = function(dataMain, dataNearby) {
                var coordinates = [],
                    point,
                    iconSides = {
                        width : 25,
                        height: 25
                    },
                    icon,
                    iconPath;

                    
                if (dataMain.property.length == 0){
                    iconPath = that.baseUrl + 'upload/images/map_icon_empty.png'
                }
                else {
                    iconPath = dataMain.property[0].icon.files[0].path;
                }

                icon = {
                    scaledSize  : new google.maps.Size(50, 50),
                    url         : that.baseUrl + iconPath
                };

                point = {
                    position : new google.maps.LatLng(dataMain['latitude'], dataMain['longitude']),
                    icon     : icon,
                    title    : dataMain.name,
                    area     : dataMain.area[0].name,
                    itemid   : dataMain.id
                };

                coordinates.push(point);
                for(var i = 0; i < dataNearby.length; i++){
                    if (dataNearby[i].property.length == 0){
                        iconPath = that.baseUrl + 'upload/images/map_icon_empty.png'
                    }
                    else {
                        iconPath = dataNearby[i].property[0].icon.files[0].path;
                    }

                    icon = {
                        scaledSize  : new google.maps.Size(iconSides.width, iconSides.height),
                        url         : that.baseUrl + iconPath
                    };

                    point = {
                        position : new google.maps.LatLng(dataNearby[i]['latitude'], dataNearby[i]['longitude']),
                        icon     : icon,
                        title    : dataNearby[i].name,
                        area     : dataNearby[i].area[0].name,
                        itemid   : dataNearby[i].id
                    };

                    coordinates.push(point);
                };

                return coordinates;
            };

            var setCenter = function(coordinates, map) {
                var center = new google.maps.LatLng(coordinates.lat, coordinates.lng);
                    map.setCenter(center);
                    console.log(map.getBounds().toString());
            };
                    
            var setBounds = function(coordinates, map) {
                var bounds = new google.maps.LatLngBounds();
                coordinates.forEach(function(point){
                    bounds.extend(point.position);
                });

                google.maps.event.trigger(map, 'resize');
                map.fitBounds(bounds);
            };

            var setMarkers = function(coordinates, map) {
                var marker,
                    infowindow = new google.maps.InfoWindow();

                coordinates.forEach(function(point){
                    var baseLocation = $location.absUrl().substring(0, $location.absUrl().indexOf('?'));
                    var contentString =
                        '<div id="content">'+
                            '<div id="siteNotice"></div>'+
                            '<div class="show-onmap-btn custom-gm-style"><a href="' + baseLocation + '/' + point.itemid + '">' + point.title + '</a></div>'+
                            '<div id="bodyContent">'+
                            '</div>'+
                        '</div>';

                    marker = new google.maps.Marker({
                        position    : point.position,
                        map         : map,
                        icon        : point.icon
                    });

                    google.maps.event.addListener(marker, 'click', (function(marker) {
                        return function() {
                            infowindow.setContent(contentString);
                            infowindow.open(map, marker);
                        }
                    })(marker));
                });
            };

            var setMap = function(dataMain, dataNearby, map) {
                var coordinates = parseCoordinates(dataMain, dataNearby);
                setBounds(coordinates, map);
                setMarkers(coordinates, map);
                var center = {
                    lat: dataMain['latitude'], 
                    lng: dataMain['longitude']
                }
                setCenter(center, map);
                map.setOptions({zoom: 17});
            };
           
          
           
    }]
);