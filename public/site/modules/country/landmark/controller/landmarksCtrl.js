app.filter('withoutCitiesFilter', function() {
    return function(areas, apply) {
        if(parseInt(apply)) {
            for(var k in areas) {
                if (areas.hasOwnProperty(k)) {
                    if(parseInt(areas[k].area_type) == 2){
                        areas.splice(k, 1);
                    }
                }
            }
        }
        return areas;
    }
});
/**
 *
 * @param mainConfig
 * @param phrases
 * @param countryLandmarks
 * @param $stateParams
 * @param landmarkProperties
 * @param generalInfo
 * @param $scope
 * @param $location
 * @param areaEntityList
 */

app.controller('landmarksCtrl', ['mainConfig', 'phrases', 'areaEntityList', 'countryLandmarks', 'landmarksHal', 'landmarkProperties', 'generalInfo',
    '$stateParams', '$scope', '$location',
    function (mainConfig, phrases, areaEntityList, countryLandmarks, landmarksHal, landmarkProperties, generalInfo, $stateParams,
                           $scope, $location) {
        var that            = this,
            countryAlias    = $stateParams.countryAlias,
            isVicinity      = ($stateParams.v == 'vicinity' ? 1 : 0),
            fieldsArray     = [
                'name',
                'image.files',
                'property.icon.files',
                'area.name'
            ],
            fields          = fieldsArray.join(),
            queryFilter = [
                'area:eq:' + countryAlias,// + '*area.landmark_area.vicinity:eq:'+isVicinity,
                // 'regional:eq:'+isVicinity,
                'status:eq:1'
            ],
            filter = queryFilter.join(mainConfig.filterSeparator),
            pageNumber      = 1,
            sorting         = {
                1: 'id:asc',
                2: 'rating:desc',
                3: 'updated_at:desc'
            };
        that.map_load = false;
        that.baseUrl          = mainConfig.basePublicUrl;
        $scope.additionalFilters = [];
        $scope.mapFilters = 0;
        that.currentOrder     = sorting[2]; // сортировка по умолчанию
        that.moreButtonText   = phrases.pageMore;
        that.lowerFilters     = [];
        that.pagesize         = mainConfig.pagesize;
        that.total            = 0;
        
        that.marker = [];
        that.isVicinity = isVicinity;
        that.showGrid = false;
        that.isInitQuery = true;
        
        var queryParams = {
            filter      : filter,
            fields      : fields,
            pagesize    : that.pagesize,
            page        : pageNumber,
            order       : sorting[2]
        };

        var area_promise = generalInfo.query(
            {
                area_id: countryAlias,
                fields: 'landmark_text'
            },
            function(data) {
                that.entity_text = data.landmark_text;
                that.area_t = data.area_type;
            }
        );

                    var parseCoordinates = function(data)
                    {
                        var coordinates = [],
                            point,
                            iconSides = {
                                width : 25,
                                height: 25
                            },
                            icon,
                            iconPath;

                        data.forEach(function(item) {
                            if (item.property.length == 0){
                                iconPath = mainConfig.basePublicUrl + 'upload/images/map_icon_empty.png'
                            }
                            else {
                                iconPath = item.property[0].icon.files[0].path;
                            }

                            icon = {
                                scaledSize  : new google.maps.Size(iconSides.width, iconSides.height),
                                url         : mainConfig.basePublicUrl + iconPath
                            };

                            point = {
                                position : new google.maps.LatLng(item['latitude'], item['longitude']),
                                icon     : icon,
                                title    : item.name,
                                area     : item.area[0].name,
                                itemid   : item.id
                            };

                            coordinates.push(point);
                        });

                        return coordinates;
                    };

                    var setBounds = function(coordinates, map)
                    {
                        that.marker.forEach(function(item){
                            item.setMap(null);
                        });
                        var bounds = new google.maps.LatLngBounds();

                        coordinates.forEach(function(point){
                           bounds.extend(point.position);
                        });

                        google.maps.event.trigger(map, 'resize');
                        map.fitBounds(bounds);
                    };

                    //var setCenter = function(coordinates, map) {
                    //    var center = map.getCenter(coordinates);
                    //    map.setCenter(center);
                    //
                    //    console.log('setting center done');
                    //};

                    var setMarkers = function(coordinates, map) {
                        
                        var /*marker = [],*/
                            infowindow = new google.maps.InfoWindow();
                        var i = 0;
                        coordinates.forEach(function(point){
                            // нужно улучшить формирование url-а достопримечательности
                            var baseLocation = $location.absUrl().substring(0, $location.absUrl().indexOf('?'));

                            var contentString =
                                '<div id="content">'+
                                    '<div id="siteNotice"></div>'+
                                    '<div class="show-onmap-btn custom-gm-style"><a href="' + baseLocation + '/' + point.itemid + '">' + point.title + '</a></div>'+
                                    '<div id="bodyContent">'+
                                    '</div>'+
                                '</div>';

                            that.marker.push(new google.maps.Marker({
                                position    : point.position,
                                map         : map,
                                icon        : point.icon
                            }));
                            
                            google.maps.event.addListener(that.marker[i], 'click', (function(marker) {
                                return function() {
                                    infowindow.setContent(contentString);
                                    infowindow.open(map, marker);
                                }
                            })(that.marker[i]));
                            i++;
                        });
                    };

                    var setMap = function(data, map) {
                        
                        var coordinates = parseCoordinates(data);
                            
                        setBounds(coordinates, map);
                        setMarkers(coordinates, map);
                        
                        
                    };

                    $scope.$on('mapInitialized', function(event, map) {
                        
                        
                        
                        that.mapSubFilters = false;
                        setMap(that.landmarks, map);
                        
                        
                        that.fromList = function(){
                            that.map_load = true;
                            that.mapSubFilters = false;
                            $scope.mapFilters = 0;
                            setMap(that.landmarks, map);
                            that.map_load = false;
                        };
                        
                        that.allFandmarksFilter = function(){
                            that.map_load = true;
                            that.mapSubFilters = true;
                            $scope.mapFilters = 9;
                            filter = queryFilter.join(mainConfig.filterSeparator),
                            landmarksHal.query(
                                {
                                    filter      : filter,
                                    fields      : ['property.icon.files','area.name'].join(),
                                    pagesize    : 500,
                                    page        : pageNumber,
                                    order       : sorting[2]
                                },function(data){
                                    setMap(data.results, map);
                                    that.map_load = false;
                                }
                            );
                        };
                        
                        that.mapFilter = function(idProperty){
                            that.map_load = true;
                            $scope.mapFilters = idProperty;
                            filter = queryFilter.join(mainConfig.filterSeparator);
                            var temp = [];
                            temp.push('property.id:eq:'+idProperty.toString());
                            //queryFilter.concat(temp).join(';').toString();
                            filter = filter +';'+ temp.toString();
                            landmarksHal.query(
                                {
                                    filter      : filter,
                                    fields      : ['property.icon.files','area.name'].join(),
                                    pagesize    : 500,
                                    page        : pageNumber,
                                    order       : sorting[2]
                                },function(data){
                                    setMap(data.results, map);
                                    that.map_load = false;
                                }
                            );
                            
                        };
                        
                    });
        
        

        // init query
        // take first <pagesize> landmarks
        area_promise.$promise.then(function(data){
            if(data.area_type == 2)
            {
                queryFilter.push('regional:eq:'+isVicinity);
                queryParams.filter = queryFilter.join(mainConfig.filterSeparator);
            }
            
            landmarksHal.query(
                queryParams,
                function (data) {
                    that.landmarks  = data.results;
                    that.total      = data.total;

                    $scope.$on('masonry.loaded', function (scope, element, attrs) {
                        if (that.isInitQuery)
                        {
                            that.showGrid = true;
                            that.isInitQuery = false;
                        }
                    });
                });
        });


        that.showFilter = false;

        // reset filter on hide block
        $scope.$watch(
            "showFilter",
            function handleFooChange( newValue, oldValue ) {
                if(oldValue && !newValue) {
                    that.resetFilter();
                }
            }
        );

        var mergeWithAdditionalFilters = function () {
            var temp = [];
            for (var one in $scope.additionalFilters) {
                if($scope.additionalFilters.hasOwnProperty(one))
                    temp.push('property.id:eq:'+$scope.additionalFilters[one]);
            }
            return queryFilter.concat(temp).join(';').toString();
        };

        that.loadMore = function() {
            pageNumber++; // следующий номер страницы

            queryParams.page        = pageNumber;
            queryParams.order       = that.currentOrder;
            queryParams.pagesize    = that.pagesize;
            queryParams.filter      = mergeWithAdditionalFilters();

            landmarksHal.query(
                queryParams,
                function (data) {
                    that.total      = data.total;
                    that.landmarks  = that.landmarks.concat(data.results);
                });
        };

        that.orderBy = function(sortId) {
            that.currentOrder = sorting[sortId];

            pageNumber = 1; // сброс всех страниц, открытых кнопкой "Ещё"

            queryParams.page    = pageNumber;
            queryParams.order   = that.currentOrder;

            landmarksHal.query(
                queryParams,
                function (data) {
                    that.total       = data.total;
                    that.landmarks  = data.results;
                });
        };

        // set sub filter
        var loadLowerProperties = function(upper_property_id) {
            var subQueryParams = {
                property_id : upper_property_id,
                fields      : 'lower'
            };

            landmarkProperties.query(
                subQueryParams,
                function(properties) {
                    // that.lowerFilters = that.lowerFilters.concat(properties.lower);
                    that.lowerFilters = properties.lower;
                }
            );
        };

        that.resetFilter = function() {
            that.showGrid = false;
            $scope.additionalFilters = [];

            // apply base fields and base filter
            pageNumber = 1;
            queryParams.page = pageNumber;
            queryParams.fields  = fields;
            queryParams.filter = filter;
            queryParams.pagesize = that.pagesize;

            landmarksHal.query(
                queryParams,
                function (data) {
                    that.total      = data.total;
                    that.landmarks  = data.results;
                    that.lowerFilters = [];

                    $scope.$on('masonry.loaded', function (scope, element, attrs) {
                        if (!that.isInitQuery) {
                            that.showGrid = true;
                        }
                    });
                });
        };

        // фильтрация достопримечательностей
        that.addFilter = function(property_id, level) {
            that.showGrid = false;

            // reset to first page
            pageNumber = 1;
            queryParams.page = pageNumber;
            // add 'property' field to select
            queryParams.fields  = fieldsArray.concat('property').join(',').toString();
            queryParams.pagesize = that.pagesize;

            // miss coffeescript :)
            if(level == null)
                level = 0;

            $scope.additionalFilters = $scope.additionalFilters.slice(0, level);
            $scope.additionalFilters[level] = property_id;

            queryParams.filter = mergeWithAdditionalFilters();

            landmarksHal.query(
                queryParams,
                function (data) {
                    that.total      = data.total;
                    that.landmarks  = data.results;

                    if (!level)
                    {
                        loadLowerProperties(property_id);
                    }

                    $scope.$on('masonry.loaded', function (scope, element, attrs) {
                        if (!that.isInitQuery) {
                            that.showGrid = true;
                        }
                    });

                });
        };
    }
]);