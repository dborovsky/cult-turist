app.controller('mainController', ['generalInfo', '$stateParams', 'mainConfig', '$sce', 'landmarksHal',
    'areaEntityList', 'entityDirectList','$scope', '$timeout', 'phrases',
    function (generalInfo, $stateParams, mainConfig, $sce, landmarksHal, areaEntityList, entityDirectList, $scope, $timeout, phrases) {
        var that            = this,
            countryAlias    = $stateParams.countryAlias,
            fieldsArray     = [
                'image',
                'gallery'
            ],
            fields          = fieldsArray.join(),
            entitesNames = ['landmark', 'people', 'feature', 'kitchen', 'event'],
            lmFieldsArray     = [
                'image.files',
                'property.icon.files',
                'area.upper'
            ],
            lmFields          = lmFieldsArray.join(),
            orderBy           = 'rating:desc',
            listSize          = 3,
            listSizelm        = 7;

        that.moreButtonText = phrases.pageMore;
        that.baseUrl  = mainConfig.basePublicUrl;
        that.showGrid = false;
        that.showSticked = false;
        that.recentArts = [];
        that.stickedArts = [];
        that.page = 1;
        that.total = 0;
        
        that.area_type = 0;
        //that.landmarkTotalRait = 0;
        
        function shuffle(a) {
            var j, x, i;
            for (i = a.length; i; i--) {
                j = Math.floor(Math.random() * i);
                x = a[i - 1];
                a[i - 1] = a[j];
                a[j] = x;
            }
        }
        
        /*function refresh() {
            // We need to give Masonry a little jump-start, otherwise the bricks
            // will render in one big overlapped stack sometimes
            common.$timeout(function () { $scope.$broadcast('masonry.reload'); }, 100);
        }*/
        
        var setFields = function(entityName) {
            var entityFieldsArray     = [
                'web_image.files',
                'original_name',
                'area.name',
                'area.upper'
            ];

            return entityFieldsArray.join();
        };

        var setFilter = function(entityName, sticked) {
            var stick = '';
            if(that.area_type == 2){
                stick = 'sticky_city';
            }else
            if(that.area_type == 1){
                stick = 'sticky_region';
            }else
            if(that.area_type == 0){
                stick = 'sticky_country';
            }
            
            var filter = [
                'area.id:eq:' + countryAlias,
                'status:eq:1',
                'area.system:eq:0',
                 stick + ':eq:' + +sticked
            ];

            return filter.join(mainConfig.filterSeparator);
        };

        var setEntityQuery = function(entityName, sticked) {
            return {
                entity  : entityName,
                filter  : setFilter(entityName, sticked),
                fields  : setFields(entityName),
                pagesize: listSize,
                page    : that.page,
                order   : orderBy
            }
        };

        var setLandmarkQuery = function(sticked) {
            var stick = '';
            
            if(that.area_type == 2){
                stick = 'sticky_city';
            }else
            if(that.area_type == 1){
                stick = 'sticky_region';
            }else
            if(that.area_type == 0){
                stick = 'sticky_country';
            }
            
            var filter = [
                'area.id:eq:' + countryAlias,
                'area.system:eq:0',
                stick + ':eq:' + +sticked
            ];

            filter = filter.join(mainConfig.filterSeparator);

            return {
                filter      : filter,
                fields      : lmFields,
                page        : that.page,
                pagesize    : listSizelm,
                order       : 'rating:desc'
            }
        };

        // формирование списка свежих статей
        var addEntities = function(array, sticked) {
            var groupType = '',
                result = [];

            for (var key in array) {
                groupType = key;
                if (array.hasOwnProperty(key)) {
                    array[key].forEach(function (item) {
                        item.entity = groupType;
                        result.push(item);
                    });
                }
            }

            var sliceLength;

            if (sticked) {
                sliceLength =  mainConfig.areaPageStickedListSize;
            }
            else {
                sliceLength =  mainConfig.areaPageArticleListSize;
            }

            result.sort(sortByDate);
            result = result.slice(0, sliceLength);

            return result;
        };
        /*var groupAllEnt = function(arr){
            var resultArr = [];
            for(var i = 0; i < 4; i++){
                for(var j = 0; j < arr[entitesNames[i]].length; j++){
                    arr[entitesNames[i]][j].entity = entitesNames[i];
                    resultArr.push(arr[entitesNames[i]][j]);
                }
            }
            return resultArr;
        };*/
        
        var sortByDate = function(a, b){
            var dateA = new Date(a.updated_at),
                dateB = new Date(b.updated_at);

            return dateB - dateA; //sort by date descending
        };
        
        that.getMainPageArticles = function(sticked) {
            
            that.dummy = [];
            that.dummy['landmark'] = [];
            that.dummy['people'] = [];
            that.dummy['feature'] = [];
            that.dummy['kitchen'] = [];
            that.dummy['event'] = [];
            
            landmarksHal.query(
                setLandmarkQuery(sticked),
                function (data) {
                    //that.lmPage = that.landmarkTotalRait / 5;
                    for(var i = 0; i< data.results.length; i++ ){
                        that.dummy[entitesNames[0]].push(data.results[i]);
                    }
                    that.total += data.total;
                })
                .$promise.then(function () {
                    // теперь люди, фишки, кухни
                    var entityName = entitesNames[1];
                    entityDirectList.query(
                        setEntityQuery(entityName, sticked),
                        function (data) {
                            for(var i = 0; i< data.results.length; i++ ){
                                that.dummy[entityName].push(data.results[i]);
                            }
                            that.total += data.total;
                        })
                        .$promise.then(function () {
                            var entityName = entitesNames[2];
                            entityDirectList.query(
                                setEntityQuery(entityName, sticked),
                                function (data) {
                                    for(var i = 0; i< data.results.length; i++ ){
                                        that.dummy[entityName].push(data.results[i]);
                                    }
                                    that.total += data.total;
                                })
                                .$promise.then(function () {
                                    var entityName = entitesNames[3];
                                    entityDirectList.query(
                                        setEntityQuery(entityName, sticked),
                                        function (data) {
                                            for(var i = 0; i< data.results.length; i++ ){
                                                that.dummy[entityName].push(data.results[i]);
                                            }
                                            that.total += data.total;
                                        }).$promise.then(function(){
                                                var entityName = entitesNames[4];
                                                entityDirectList.query(
                                                setEntityQuery(entityName, sticked),
                                                function (data) {
                                                    for(var i = 0; i< data.results.length; i++ ){
                                                        that.dummy[entityName].push(data.results[i]);
                                                    }
                                                    that.total += data.total;
                                                }).$promise.then(function () {
                                                    if (sticked) {
                                                        that.stickedArts = addEntities(that.dummy, sticked);
                                                    }
                                                    else {
                                                        that.recentArts = addEntities(that.dummy, sticked);
                                                        shuffle(that.recentArts);
                                                    }

                                                    $scope.$on('masonry.loaded', function () {
                                                        if (sticked) {
                                                            that.showSticked = true;
                                                        }
                                                        else {
                                                            that.showGrid = true;
                                                            $timeout(function () {
                                                                $scope.$broadcast('masonry.reload');
                                                            }, 500);
                                                        }  
                                                    });
                                                 });
                                                
                                             });
                                    });
                                });
                });
        };
        
        that.getStickedPageArticles = function(sticked) {
            
            that.dummy1 = [];
            that.dummy1['landmark'] = [];
            that.dummy1['people'] = [];
            that.dummy1['feature'] = [];
            that.dummy1['kitchen'] = [];
            that.dummy1['event'] = [];
            
            landmarksHal.query(
                setLandmarkQuery(sticked,listSize),
                function (data) {
                    for(var i = 0; i< data.results.length; i++ ){
                        that.dummy1[entitesNames[0]].push(data.results[i]);
                    }
                })
                .$promise.then(function () {
                    // теперь люди, фишки, кухни
                    var entityName = entitesNames[1];
                    entityDirectList.query(
                        setEntityQuery(entityName, sticked),
                        function (data) {
                            for(var i = 0; i< data.results.length; i++ ){
                                that.dummy1[entityName].push(data.results[i]);
                            }
                        })
                        .$promise.then(function () {
                            var entityName = entitesNames[2];
                            entityDirectList.query(
                                setEntityQuery(entityName, sticked),
                                function (data) {
                                    for(var i = 0; i< data.results.length; i++ ){
                                        that.dummy1[entityName].push(data.results[i]);
                                    }
                                })
                                .$promise.then(function () {
                                    var entityName = entitesNames[3];
                                    entityDirectList.query(
                                        setEntityQuery(entityName, sticked),
                                        function (data) {
                                            for(var i = 0; i< data.results.length; i++ ){
                                                that.dummy1[entityName].push(data.results[i]);
                                            }
                                        })
                                        .$promise.then(function () {
                                            var entityName = entitesNames[4];
                                            entityDirectList.query(
                                                setEntityQuery(entityName, sticked),
                                                function (data) {
                                                    for(var i = 0; i< data.results.length; i++ ){
                                                        that.dummy1[entityName].push(data.results[i]);
                                                    }
                                            })
                                            .$promise.then(function () {
                                                if (sticked) {
                                                    var flag = false;
                                                    for(var i = 0; i < 5; i++){
                                                        for(var j = 0; j < that.dummy1[entitesNames[i]].length; j++){
                                                            flag = false;
                                                            for(var l = 0; l < that.dummy1[entitesNames[i]][j].area.length; l++){
                                                                if(that.dummy1[entitesNames[i]][j].area[l].system == 0 
                                                                    && that.dummy1[entitesNames[i]][j].area[l].id == countryAlias){
                                                                       flag = true; 
                                                                }
                                                            }
                                                            if(flag == false){
                                                                delete that.dummy1[entitesNames[i]][j];
                                                            }
                                                        }
                                                    }
                                                    that.stickedArts = addEntities(that.dummy1, sticked);
                                                }
                                                else {
                                                    that.recentArts = addEntities(that.dummy1, sticked);
                                                }

                                                $scope.$on('masonry.loaded', function () {
                                                    if (sticked) {
                                                        that.showSticked = true;
                                                    }
                                                    else {
                                                        that.showGrid = true;
                                                    }
                                                });
                                        });
                                });
                        });
                })
            });
        }
        that.loadmore = function(sticked) {
            if (sticked == null) {
                sticked = false;
            }

            that.dummy = [];
            that.dummy['landmark'] = [];
            that.dummy['people'] = [];
            that.dummy['feature'] = [];
            that.dummy['kitchen'] = [];
            that.dummy['event'] = [];
            
            that.page++;
            that.lmPage++;

            landmarksHal.query(
                setLandmarkQuery(sticked),
                function (data) {
                    for(var i = 0; i< data.results.length; i++ ){
                        that.dummy[entitesNames[0]].push(data.results[i]);
                    }
                })
                .$promise.then(function () {
                    // теперь люди, фишки, кухни
                    var entityName = entitesNames[1];
                    entityDirectList.query(
                        setEntityQuery(entityName, sticked),
                        function (data) {
                            for(var i = 0; i< data.results.length; i++ ){
                                that.dummy[entityName].push(data.results[i]);
                            }
                        })
                        .$promise.then(function () {
                            var entityName = entitesNames[2];
                            entityDirectList.query(
                                setEntityQuery(entityName, sticked),
                                function (data) {
                                    for(var i = 0; i< data.results.length; i++ ){
                                        that.dummy[entityName].push(data.results[i]);
                                    }
                                })
                                .$promise.then(function () {
                                    var entityName = entitesNames[3];
                                    entityDirectList.query(
                                        setEntityQuery(entityName, sticked),
                                        function (data) {
                                            for(var i = 0; i< data.results.length; i++ ){
                                                that.dummy[entityName].push(data.results[i]);
                                            }
                                    })
                                    .$promise.then(function () {
                                        var entityName = entitesNames[3];
                                        entityDirectList.query(
                                        setEntityQuery(entityName, sticked),
                                            function (data) {
                                                for(var i = 0; i< data.results.length; i++ ){
                                                    that.dummy[entityName].push(data.results[i]);
                                                }
                                            })
                                            .$promise.then(function () {
                                                if (sticked) {
                                                    that.stickedArts = addEntities(that.dummy, sticked);
                                                }
                                                else {
                                                    //that.recentArts = addEntities(that.dummy, sticked);
                                                    //that.recentArts = that.recentArts.concat(groupAllEnt(that.dummy));
                                                    var tmp;
                                                    tmp = addEntities(that.dummy, sticked);
                                                    shuffle(tmp);
                                                    that.recentArts = that.recentArts.concat(tmp);
                                                }

                                                $scope.$on('masonry.loaded', function (element) {
                                                    if (sticked) {
                                                        that.showSticked = true;
                                                    }
                                                    else {
                                                        that.showGrid = true;
                                                        $timeout(function () {
                                                            $scope.$broadcast('masonry.reload');
                                                        }, 500);
                                                    }
                                                });
                                        });
                                    });
                                });
                        });
                });
        };
        
        
        // этот сервис использовался для формирования ссылки на карту с границами area
        // https://developers.google.com/maps/documentation/embed/start
        generalInfo.query(
            {
                area_id : countryAlias,
                fields  : fields
            },
            function(data) {
                that.mainInfo           = data;
                that.area_type          = data.area_type;
                that.googleMapsEmbedUrl = $sce.trustAsResourceUrl(
                    'https://www.google.com/maps/embed/v1/place?q=' +
                    data.name +
                    '&key=' +
                    mainConfig.googleMapsApiKey);
        }).$promise.then(function(){
                that.getStickedPageArticles(true);
                that.getMainPageArticles(false);
            });
    }
]);