/**
 *
 * @param $resource
 * @param mainConfig
 * @returns {*}
 */

app.factory("general", ['$resource', 'mainConfig',
    function ($resource, mainConfig) {
        return $resource(
            mainConfig.apiUrl + ':entity/:area_id',
            {
                entity:     'area',
                area_id:    ':area_id',
                fields:     ':fields'
            },
            {
                query: {
                    method: "GET"
                }
            }
        );
    }
]);