app.controller('bottomBlockController', ['bottomBlock', '$stateParams', 'mainConfig',
    function (bottomBlock, $stateParams, mainConfig) {
        var that            = this,
            entityName      = $stateParams.entityName,
            entityItemId    = $stateParams.entityItemId,
            countryAlias    = $stateParams.countryAlias,
            fieldsArray     = [
                'name',
                'area'
            ],
            fields;
        //
        // if (entityName === 'landmark'){
        //     fieldsArray.push('property.icon.files');
        // } else if (entityName === 'feature') {
        //     fieldsArray.push('web_image.files');
        // } else if (entityName === 'feature') {
        //     fieldsArray.push('image.files');
        // }
        that.baseUrl = mainConfig.basePublicUrl;

        if($stateParams.landmarkId !==undefined){
            entityName = 'landmark';
            entityItemId = $stateParams.landmarkId;
        }
        fields = fieldsArray.join();

        bottomBlock.query(
            {
                entity_name : entityName,
                entity_id   : entityItemId,
                area_id     : countryAlias,
                fields      : fields
            },
            function(data) {
                that.entityName = entityName;
                that.data = data.data;
            }
        );
    }
]);