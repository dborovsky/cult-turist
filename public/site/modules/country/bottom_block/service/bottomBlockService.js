/**
 *
 * @param $resource
 * @param mainConfig
 * @returns {*}
 */
app.factory("bottomBlock", ['$resource', 'mainConfig',
    function ($resource, mainConfig) {
        return $resource(
            mainConfig.apiUrl + 'maybeinteresting',
            {
                entity_name : ':entity_name',
                entity_id: ':entity_id',
                area_id: ':area_id',
                fields   : ':fields'
            },
            {
                query: {
                    method: "GET"
                }
            }
        );
    }
]);