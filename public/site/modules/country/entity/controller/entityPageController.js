app.controller('entityPageController', ['entityInfo', '$stateParams', 'mainConfig', 'constants',
    function (entityInfo, $stateParams, mainConfig, constants) {
        var that            = this,
            entityName 		= $stateParams.entityName,
            entityItemId    = $stateParams.entityItemId,
            fieldsArray     = [
                'text',
                'area',
                'detail',
                'image.files'
                ],
            fields,
            filter          = 'area.system:eq:0';

        that.entityName = $stateParams.entityName;
        that.baseUrl = mainConfig.basePublicUrl;
    	fieldsArray.push('web_image.files');
    	that.maxImageWidth = constants.featureImageMaxWidth;
    	that.maxAbsoluteImageWidth = constants.featureImageAbsoluteMaxWidth;
    	that.maxAbsoluteImageHeight = constants.featureImageAbsoluteMaxHeight;
        fields = fieldsArray.join();
        entityInfo.query(
            {
                entity_name : entityName,
                entity_id   : entityItemId,
                fields      : fields,
                filter      : filter
            },
            function(data) {
                that.entityInfo = data;
                var filesLength = data.web_image.files.length;
                that.origImageIndex = filesLength - 1;
                for (var i = 0; i < filesLength; i++) {
                	var curIndex = filesLength - i - 1;
                	if (data.web_image.files[curIndex]['width'] <= that.maxAbsoluteImageWidth) {
                		if (data.web_image.files[curIndex]['height'] <= that.maxAbsoluteImageHeight) {
                			that.fullImageIndex = curIndex;
                			break;
                		}
                	}
                }
            }
        );
    }
]);