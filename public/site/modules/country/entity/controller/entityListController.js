/**
 *
 * @param mainConfig
 * @param areaEntityList
 * @param $stateParams
 * @param generalInfo
 * @param phrases
 * @param $scope
 * @param $q
 */
app.controller('entityListController', ['mainConfig', 'areaEntityList', 'generalInfo', '$stateParams', 'phrases',
    function (mainConfig, areaEntityList, generalInfo, $stateParams, phrases) {
        var that            = this,
            countryAlias    = $stateParams.countryAlias,
            entityName      = $stateParams.entityName,
            fieldsArray     = [
                entityName,
                entityName + '.web_image.files',
                entityName + '.original_name'
            ],
            fields          = fieldsArray.join(),
            filterArray     = [
                entityName + '.status:eq:1',
                entityName + '.system:eq:0'

            ],
            order               = entityName + '.id:desc',
            filter              = filterArray.join(mainConfig.filterSeparator),
            relatedItemsCount   = 4,
            areaBinding         = [];

        that.entity             = [];
        that.baseUrl            = mainConfig.basePublicUrl;
        that.moreButtonText     = phrases.pageMore;
        that.pagesize           = mainConfig.pagesize;
        that.entityName         = entityName;
        that.pagesize           = mainConfig.pagesize;
        that.showGrid           = false;
        that.alsoBeIntresting   = phrases.alsoBeIntresting;

        // kitchen filter:
        that.curAreaType        = null;
        that.kitchenTypes       = {
            1: {alias: 'disheskitchen', title: 'Блюдо'},
            2: {alias: 'drinkskitchen', title: 'Напиток'},
            3: {alias: 'productskitchen', title: 'Продукт'}
        };
        that.kitchenGeos         = [1,2];
        that.selectedGeos        = [1,2];
        that.selectedTypes       = Object.keys(that.kitchenTypes);

        that.isFullList          = JSON.stringify(that.kitchenGeos) == JSON.stringify(that.selectedGeos) && JSON.stringify(Object.keys(that.kitchenTypes)) == JSON.stringify(that.selectedTypes);

        var applySelectedTypes = function () {
            if(entityName == 'kitchen') {
                filter = [entityName + '.type:in:' + that.selectedTypes.join(',')].concat(filterArray).join(mainConfig.filterSeparator);
            }
        };

        applySelectedTypes();

        that.toggleGeo = function (type) {
            var index = that.selectedGeos.indexOf(type);
            if(index != -1)
            {
                if(that.selectedGeos.length > 1){
                    that.selectedGeos.splice(index, 1);
                    that.isFullList          = JSON.stringify(that.kitchenGeos) == JSON.stringify(that.selectedGeos) && JSON.stringify(Object.keys(that.kitchenTypes)) == JSON.stringify(that.selectedTypes);
                    loadEntities();
                }
            }
            else
            {
                that.selectedGeos.push(type);
                that.isFullList          = JSON.stringify(that.kitchenGeos) == JSON.stringify(that.selectedGeos) && JSON.stringify(Object.keys(that.kitchenTypes)) == JSON.stringify(that.selectedTypes);
                loadEntities();
            }
        };

        that.toggleType = function (type) {
            var index = that.selectedTypes.indexOf(type);
            if(index != -1)
            {
                if(that.selectedTypes.length > 1){
                    that.selectedTypes.splice(index, 1);
                    that.isFullList          = JSON.stringify(that.kitchenGeos) == JSON.stringify(that.selectedGeos) && JSON.stringify(Object.keys(that.kitchenTypes)) == JSON.stringify(that.selectedTypes);
                    loadEntities();
                }
            }
            else
            {
                that.selectedTypes.push(type);
                that.isFullList          = JSON.stringify(that.kitchenGeos) == JSON.stringify(that.selectedGeos) && JSON.stringify(Object.keys(that.kitchenTypes)) == JSON.stringify(that.selectedTypes);
                loadEntities();
            }
        };
        // end kitchen filter

        var areaTypeSort = function(a, b) {
            if(a.area_type < b.area_type)
                return 1;

            if(a.area_type > b.area_type)
                return -1;

            return 0;
        };

        var getQueryParams = function(area_id) {
            return {
                area_id : area_id,
                filter  : filter,
                fields  : fields,
                order   : order,
                pagesize: mainConfig.pageSize
            }
        };

        var arrayUnique =  function(array) {
            var a = array.concat();
            for(var i=0; i<a.length; ++i) {
                for(var j=i+1; j<a.length; ++j) {
                    if(a[i].id === a[j].id)
                        a.splice(j--, 1);
                }
            }
            return a;
        };

        generalInfo.query(
            {
                area_id: countryAlias,
                fields : entityName + '_text,upper'
            },
            function(data) {
                var dummy;

                that.entity_text = data[entityName + '_text'];

                dummy = {
                    'id'        : data.id,
                    'area_type' : data.area_type
                };
                that.curAreaType = data.area_type;
                areaBinding.push(dummy);

                data.upper.forEach(function(area){
                    dummy = {
                        'id'        : area.id,
                        'area_type' : area.area_type
                    };
                    areaBinding.push(dummy);
                });

                areaBinding.sort(areaTypeSort);
            }
        ).$promise.then(function(){
            loadEntities();
        });

        var loadEntities = function () {
            applySelectedTypes();

            areaEntityList.query(
                getQueryParams(areaBinding[0].id),
                function (data) {
                    // that.entity = that.entity.concat(data[entityName]);
                    // only for kitchen
                    if(that.selectedGeos.indexOf(2)!=-1)
                    {
                        that.entity = data[entityName];
                        if (areaBinding.length == 1) {
                            that.showGrid       = true;
                            that.relatedItems   = getRelatedItems(that.entity);
                        }
                    }
                    else
                    {
                        that.entity = [];
                        if (areaBinding.length == 1) {
                            that.showGrid       = true;
                            that.relatedItems   = getRelatedItems(that.entity);
                        }
                    }
                }).$promise.then(function(){
                    if(areaBinding[1] != undefined && (that.selectedGeos.indexOf(1)!=-1) )
                        areaEntityList.query(
                            getQueryParams(areaBinding[1].id),
                            function (data) {
                                that.entity = arrayUnique(that.entity.concat(data[entityName]));
                                if (areaBinding.length == 2){
                                    that.showGrid       = true;
                                    that.relatedItems   = getRelatedItems(that.entity);
                                }
                    }).$promise.then(function(){
                        if(areaBinding[2] != undefined && (that.selectedGeos.indexOf(1)!=-1) )
                            areaEntityList.query(
                                getQueryParams(areaBinding[2].id),
                                function (data) {
                                    that.entity         = arrayUnique(that.entity.concat(data[entityName]));
                                    that.showGrid       = true;
                                    that.relatedItems   = getRelatedItems(that.entity);
                                })
                });
            });
        };

        var getRelatedItems = function(items) {
            var result = [];
            // требуется улучшение алгоритма выбора связанных элементов
            if (items.length > mainConfig.pagesize) {
                result = items.slice(-relatedItemsCount);
            }

            return result;
        };
    }
]);