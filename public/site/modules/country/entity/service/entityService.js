/**
 *
 * @param $resource
 * @param mainConfig
 * @returns {*}
 */

app.factory("entityInfo", ['$resource', "mainConfig",
    function ($resource, mainConfig) {
        return $resource(
            mainConfig.apiUrl + ':entity_name/:entity_id',
            {
                entity_name : ':entity_name',
                entity_id   : ':entity_id',
                fields      : ':fields'
            },
            {
                query: {
                    method: "GET"
                }
            }
        );
    }
]);