/**
 *
 * @param $resource
 * @param mainConfig
 * @returns {*}
 */

app.factory("areaEntityList", ['$resource', 'mainConfig',
    function ($resource, mainConfig) {
        return $resource(
            mainConfig.apiUrl + ':entity/:area_id',
            {
                entity  : 'area',
                area_id : ':area_id', // значение параметра задаётся в контроллере
                fields  : ':fields',
                filter  : ':filter',
                order   : ':order'
            },
            {
                query: {
                    method: "GET"
                }
            }
        );
    }
]);