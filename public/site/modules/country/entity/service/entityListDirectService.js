/**
 *
 * @param $resource
 * @param mainConfig
 * @returns {*}
 */

app.factory("entityDirectList", ['$resource', 'mainConfig',
    function (hResource, mainConfig) {
        return hResource(
            mainConfig.apiUrl + ':entity',
            {
                entity:     ':entity',
                filter:     ':filter',
                fields:     ':fields',
                pagesize:   ':pagesize',
                order:      ':order'
            },
            {
                query: {
                    method: "GET",
                    isArray: false,
                    headers: {'Accept': 'application/hal+json, text/plain, */*'}
                }
            }
        );
    }
]);