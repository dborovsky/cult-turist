/**
 *
 * @param generalInfo
 * @param $stateParams
 */
app.controller('generalInfoCtrl', ['generalInfo', '$stateParams',
    function (generalInfo, $stateParams) {
        var that = this,
            countryAlias = $stateParams.countryAlias;

        generalInfo.query(
            {
                area_id: countryAlias,
                fields: 'detail'
            },
            function (data) {
                that.generalInfo = data.detail;
            });
    }
]);