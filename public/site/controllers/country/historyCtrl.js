
/**
 *
 * @param generalInfo
 * @param $stateParams
 */

app.controller('historyCtrl', ['generalInfo', '$stateParams',
    function (generalInfo, $stateParams) {
        var that = this,
            countryAlias = $stateParams.countryAlias;

        generalInfo.query(
            {
                area_id: countryAlias,
                fields: 'history_text'
            },
            function (data) {
                that.history_text = data.history_text;
            });
    }
]);