/**
 *
 * @param generalInfo
 * @param $stateParams
 */

app.controller('transportCtrl', ['generalInfo', '$stateParams',
    function transportCtrl(generalInfo, $stateParams) {
        var that = this,
            countryAlias = $stateParams.countryAlias;

        generalInfo.query(
            {
                area_id: countryAlias,
                fields: 'transport_text'
            },
            function (data) {
                that.transport_text = data.transport_text;
            });
    }
]);