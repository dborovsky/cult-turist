app.controller('countrypageCtrl', ['generalInfo', '$stateParams', 'mainConfig','$rootScope', '$state',
    function (generalInfo, $stateParams, mainConfig, $rootScope, $state) {
        var that = this,
            countryAlias = $stateParams.countryAlias;

        that.baseUrl = mainConfig.basePublicUrl;
        /*$rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams) {
            console.log('asdasdas');
            if (toState.name === 'countrypage.entityList') {
                // If logged out and transitioning to a logged in page:
                e.preventDefault();
                console.log('asdasdas');
                //$state.go('area_topadvert@countrypage',{reload: true});
            }
        });*/
        generalInfo.query(
            {
                area_id: countryAlias,
                fields: 'flag,upper'
            },
            function(data) {
                that.generalInfo = data;
            }
        );

        //landmarksHal.query(
        //    {
        //        //id: countryAlias
        //    },
        //    function(data) {
        //        that.lmHal = data;
        //    }
        //);
    }
]);