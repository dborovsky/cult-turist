/**
 *
 * @param $resource
 * @param mainConfig
 * @returns {*}
 */

app.factory("countries", ['$resource', 'mainConfig',
    function ($resource, mainConfig){
        return $resource(
            mainConfig.apiUrl + ':entity',
            {
                entity: 'area',
                filter: 'area_type:eq:0;status:eq:1', // 0 - страна
                fields: ':fields',
                pagesize: ':pagesize'
            },
            {
                query: {
                    method: 'GET',
                    headers: {'Accept': 'application/hal+json, text/plain, */*'}
                }
            });
    }
]);