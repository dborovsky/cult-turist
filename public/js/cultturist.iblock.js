/**
 * Iblock Model
 **/
(function($, cultturist) {
    cultturist.addModel('iblock', {
        banner: 'banner',
        name: 'name',
        
        landmark: 'landmark',
        event: 'event',
        people: 'people',
        kitchen: 'kitchen',
        feature: 'feature',
        for_all: 'for_all',
        
        fields:{
            id: { type: "number"},
            name: { type: "string", validation: { required: true}},
            partner_name: { type: "string"},
            banner_link: { type: "string"},
            widget: { type: "string"},
            status: { type: "number", defaultValue: 1, validation: {min:0, max:127, step:1}},
            position: { type: "string", defaultValue: 'top',},
            for_all: { type: "boolean", defaultValue: true},
            auto_off: { type: "boolean", defaultValue: false},
            area: { type: "values", pivot:{
                'system': { type: "boolean", defaultValue: false, title: "Системная"}
            }, multiple: false},
            
            area_flag: { type: "boolean", defaultValue: true},
            landmark: { type: "boolean", defaultValue: true},
            people: { type: "boolean", defaultValue: true},
            event: { type: "boolean", defaultValue: true},
            kitchen: { type: "boolean", defaultValue: true},
            feature: { type: "boolean", defaultValue: true},
            
            banner_id: { type: "number"},
            banner: { type: "values"},
            date_begin: { type: "date"},
            date_end: { type: "date"},
            created_at: { type: "date", untouchable: false},
            updated_at: { type: "date", untouchable: false},
        },
        columns: [
            { field: "id", title: "ID", width:60},
            { field: "name", title: "Название", sortable: false},
            { field: "banner_link", title: "Ссылка для баннера", transport: 'banner_link.ru', sortable: true, hidden:true},
            { field: "partner_name", title: "Заказчик рекламы", transport: 'partner_name.ru', sortable: true, hidden:true},
            { field: "position", title: "Позиция", width:60, hidden:true},
            { field: "for_all", title: "Общее для всех", width:60, hidden:false},
            { field: "status", title: "Статус", width:60, hidden:false},
            
            { field: "area_flag", title: "Гео", width:60, hidden:false},
            { field: "landmark", title: "Достопримечательности", width:60, hidden:false},
            { field: "people", title: "Великие люди", width:60, hidden:false},
            { field: "event", title: "События", width:60, hidden:false},
            { field: "kitchen", title: "Кухня", width:60, hidden:false},
            { field: "feature", title: "Фишки", width:60, hidden:false},
            
            { field: "auto_off", title: "Автовыключение по окончанию", hidden: true},
            { field: "banner_id", title: "bannID", width:60, hidden:true},
            { field: "banner", title: "Главная картинка", width:100, atype: 'image', transport: 'banner.path', filterable: false, sortable: false, hidden:true},
            { field: "widget", title: "Виджет", transport: 'widget.ru', sortable: false, hidden:true},
            { field: "area", title: "Страна(город, регион)", width:100, transport: 'area.id', atype: 'area', filterable: false, hidden:false, sortable: false},
            { field: "date_begin", title: "Дата запуска", format: "{0:dd.MM.yyyy}", hidden:false},
            { field: "date_end", title: "Дата выключения", format: "{0:dd.MM.yyyy}", hidden:false},
            { field: "created_at", title: "Создан", format: "{0:dd.MM.yyyy HH:mm:ss}", hidden:true},
            { field: "updated_at", title: "Обновлён", format: "{0:dd.MM.yyyy HH:mm:ss}", hidden:true},
            { command: [{ name: "destroy", text: "Удалить" },{ name: "edit", text: {
                            "edit": "Редактировать",
                            "update": "Обновить",
                            "cancel": "Отмена"
            }}], width: 160}
        ],
        template: '<ul id="panelbar">'+
            '<li class="k-state-active">'+
                '<span class="k-link k-state-selected">Основные параметры</span>'+
                '<div class="pan-in">'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">ID: </div>'+
                        '<div class="rightpin"><input type="text" readonly class="k-input k-textbox" name="id" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">Название: </div>'+
                        '<div class="rightpin"><input type="text" class="k-input k-textbox" name="name" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">Заказчик: </div>'+
                        '<div class="rightpin"><input type="text" class="k-input k-textbox" name="partner_name" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">Ссылка для банера: </div>'+
                        '<div class="rightpin"><input type="text" class="k-input k-textbox" name="banner_link" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">Статус: </div>'+
                        '<div class="rightpin"><input required data-text-field="name" data-value-field="id" name="status" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">Позиция: </div>'+
                        '<div class="rightpin"><input required data-text-field="name" data-value-field="val" name="position" /></div>'+
                    '</div>'+
                    '<div class="cellpin" style="display: none">'+
                        '<div class="leftpin">Автовыключение: </div>'+
                        '<div class="rightpin"><input type="checkbox" name="auto_off" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">Общее для всех стран: </div>'+
                        '<div class="rightpin"><input type="checkbox" name="for_all" /></div>'+
                    '</div>'+
                    '<br/><div class="cellpin">'+
                        '<div class="leftpin">Дата запуска: </div>'+
                        '<div class="rightpin"><input data-role="datepicker" data-format="{0:dd.MM.yyyy}" name="date_begin" /></div>'+
                    '</div>'+
                    '<div class="cellpin" >'+
                        '<div class="leftpin">Дата окончания: </div>'+
                        '<div class="rightpin"><input data-role="datepicker" data-format="{0:dd.MM.yyyy}" name="date_end" /></div>'+
                    '</div>'+
                    /*'<div class="cellpin">'+
                        '<div class="leftpin">Достопримечательности: </div>'+
                        '<div class="rightpin"><input type="checkbox" name="landmark" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">Великие люди: </div>'+
                        '<div class="rightpin"><input type="checkbox" name="people" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">События: </div>'+
                        '<div class="rightpin"><input type="checkbox" name="event" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">Кухня: </div>'+
                        '<div class="rightpin"><input type="checkbox" name="kitchen" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">Фишки: </div>'+
                        '<div class="rightpin"><input type="checkbox" name="feature" /></div>'+
                    '</div>'+*/
                    
                    '<br class="clear" />'+
                '</div>'+
            '</li>'+
            '<li id="geolnk">'+
                '<span class="k-link">Гео-привязки</span>'+
                '<div>'+
                    '<div id="roleArea"></div>'+
                '</div>'+
            '</li>'+
            '<li>'+
                '<span class="k-link">Постраничные привязки</span>'+
                '<div class="pan-in">'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">Гео: </div>'+
                        '<div class="rightpin"><input type="checkbox" name="area_flag" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">Достопримечательности: </div>'+
                        '<div class="rightpin"><input type="checkbox" name="landmark" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">Великие люди: </div>'+
                        '<div class="rightpin"><input type="checkbox" name="people" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">События: </div>'+
                        '<div class="rightpin"><input type="checkbox" name="event" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">Кухня: </div>'+
                        '<div class="rightpin"><input type="checkbox" name="kitchen" /></div>'+
                    '</div>'+
                    '<div class="cellpin">'+
                        '<div class="leftpin">Фишки: </div>'+
                        '<div class="rightpin"><input type="checkbox" name="feature" /></div>'+
                    '</div>'+
                    '<br class="clear" />'+
                '</div>'+
            '</li>'+
            '<li>'+
                '<span class="k-link">Iframe (виджет, скрипт)</span>'+
                '<div>'+
                    '<textarea data-role="editor" name="widget" style="width: 100%; height:300px"></textarea>'+
                    
                '</div>'+
            '</li>'+
            '<li>'+
                '<span class="k-link">Изображение (swf)</span>'+
                '<div>'+
                    '<p>Для левого блока изображения не более 240px в ширину</br>Для верхнего не более 940px в ширину</p>'+
                    '<div class="imageContainer"></div>'+
                    '<input name="banner" id="roleAdvImage" type="file" />'+
                '</div>'+
            '</li>'+/*
            '<li>'+
                '<span class="k-link">Гео объекты привязки(страны, регионы, города)</span>'+
                '<div>'+
                    '<div id="roleArea"></div>'+
                '</div>'+
            '</li>'+*/
            '</ul>',
        options: {
            status: [
                { name: "Не опубликован", id: 0 },
                { name: "Опубликован", id: 1 }
            ],
            position: [
                { name: "top", val: 'top' },
                { name: "left", val: 'left' }
            ]
        },
        templates:{
            status: function (value, model) {
                var result = '';
                $.each(model.options.status, function(index, item) {
                    if(item.id === value) {
                        result = item.name;
                    }
                });
                return result;
            },
            position: function (value, model) {
                var result = '';
                $.each(model.options.position, function(index, item) {
                    if(item.val === value) {
                        result = item.name;
                    }
                });
                return result;
            },
            area: function (value, model) {
                var result = '';
                if(value instanceof kendo.data.ObservableArray) {
                    value.forEach(function(v, k) {
                        if(k > 10) {
                            return;
                        }
                        result += v.name+"<br />";
                    });
                    if(value.length > 10) {
                        result += 'и ещё '+(value.length - 10)+'объектов.';
                    }
                }
                return result;
            }
        },
        filterables: {
            area: standartAreFiltering,
            status: function(element, model) {
                element.kendoDropDownList({
                    dataTextField: "name",
                    dataValueField: "id",
                    dataSource: {
                        data: model.options.status
                    },
                    optionLabel: "--Выберите значение--"
                });
            },
            position: function(element, model) {
                element.kendoDropDownList({
                    dataTextField: "name",
                    dataValueField: "val",
                    dataSource: {
                        data: model.options.position
                    },
                    optionLabel: "--Выберите значение--"
                });
            }
        },
        onedit: function(content, model, dataModel, panelData){
            
        /*content.find('#roleArea').eq(0).kendoRelations({
                model: model,
                modelField: 'area',
                modelData: dataModel,
                panelData: panelData,
                multiple: true,
                remote: cultturist.getModel('area')
        });*/
        
        var setVal =function(){
            var dateB =content.find('[name="date_begin"]').eq(0).kendoDatePicker().val();
            var res = dateB.split(".");
            var day   = res[0],
                month = res[1],
                year  = res[2];
        
            //var x = kendo.toString(kendo.parseDate(new Date(year,month,day), 'MM/dd/yyyy'));
            var x = new Date(year,month,day);
            x.setDate(x.getDate()+30);
            console.dir(x);
            //console.dir(x);
            x = kendo.toString(kendo.parseDate(x, 'MM/dd/yyyy'));
            $('[name="date_end"]').data("kendoDatePicker").value(x);
                
        };
        setVal();
        content.find('[name="date_begin"]').eq(0).kendoDatePicker({
            change: setVal
        });
        
        content.find('[name="date_begin"]').eq(0).bind("change", function () {
            setVal();
        });
        
        
        var areaModel = cultturist.getModel('area'),
                countrySelected = 0,
                regionSelected = 0,
                citySelected = 0,
                countries,
                regions,
                cities,
                linkButton = $('<a class="k-button k-button-icontext k-state-disabled" href="#"><span class="k-icon k-update"></span>Пересвязать</a>'),
                selectedArea = function(rel, area) {
                    if(rel.options.dataSource.get(area[areaModel.id])) {
                        return;
                    }
                    rel.options.dataSource.add({
                        id: area.id,
                        vicinity: 0,
                        distance: 0,
                        system: 0
                    });
                },
                deselectArea = function() {
                    var i    = 0,
                        tmpL = areaRel.options.dataSource._data.length;
                        for(i = 0; i < tmpL; i++){
                            //console.log(areaRel.options.dataSource._data[i].id);
                            areaRel.options.dataSource.remove(areaRel.options.dataSource._data[0]);
                        }
                        if(citySelected > 0) {
                            //deselectArea(cities, citySelected);
                            areaRel.options.dataSource.remove(citySelected);
                        }

                        if(regionSelected > 0) {
                            //deselectArea(regions, regionSelected);
                            areaRel.options.dataSource.remove(regionSelected);
                        }
                                        
                        if(countrySelected > 0) {
                            //deselectArea(countries, countrySelected);
                            areaRel.options.dataSource.remove(countrySelected);
                        }
                        //areaRel.options.modelData = [];
                        
                    /*if(item) {
                        rel.options.dataSource.remove(item);
                    }*/                    
                },
                autoChangeAfterLoad = function(rel, items) {
                    if(!(items instanceof kendo.data.ObservableArray) || items.length < 1) {
                        return;
                    }
                    var ext = items.find(function(item) {
                        var id = parseInt(item[areaModel.id]);
                        if(id > 0) {
                            return rel.options.dataSource.get(id);
                        }
                        return false;
                    });
                    
                    if(ext) {
                        this.value(ext[areaModel.id]);
                    }
                },
                toggleTools = function(rel, level, mode) {
                    (level > 0 || !mode) && cities.enable(mode);
                    level < 1 && regions.enable(mode);
                    
                    
                    if(level > 0 && mode && dataModel.regional) {
                        linkButton.hasClass('k-state-disabled') && linkButton.removeClass('k-state-disabled');
                    } else {
                        !linkButton.hasClass('k-state-disabled') && linkButton.addClass('k-state-disabled');
                    }
                },
                areaRel = content.find('#roleArea').eq(0).empty().kendoRelations({
                    model: model,
                    modelField: 'area',
                    modelData: dataModel,
                    multiple: true,
                    remote: cultturist.getModel('area'),
                    view: {
                        single: true,
                        delete: true
                    },
                    panelData: panelData,
                    tools: [
                        {
                            el: linkButton,
                            callback: function(el) {
                                var that = this;
                                
                                el.bind('click', function(e) {
                                    if(el.hasClass('k-state-disabled')) {
                                        return false;
                                    }
                                    $.ajax('/admin/roundlandmark/?id='+dataModel['id'], {
                                        type: 'GET',
                                        cache: true,
                                        dataType: "json",
                                        contentType: "application/json",
                                        success: function(r) {
                                            if(!$.isArray(r)) {
                                                return;
                                            }
                                            var items = [];
                                            r.forEach(function(item) {
                                                items.push({
                                                    id: item.id,
                                                    vicinity: 1,
                                                    distance: item.distance,
                                                    system: 1
                                                });
                                            });
                                            if(dataModel.area instanceof kendo.data.ObservableArray) {
                                                dataModel.area.forEach(function(item) {
                                                    if(item.system && item.area_type > 1) {
                                                        return;
                                                    }
                                                    items.push({
                                                        id: item.id,
                                                        vicinity: item.vicinity,
                                                        distance: item.distance,
                                                        system: 0
                                                    });
                                                });
                                            }
                                            that.options.dataSource.data(items);
                                        }
                                    });
                                    return false;
                                });
                            }
                        },                        
                        {
                            el: $('<input type="text" />'),
                            callback: function(el) {
                                var that = this;
                                cities = el.data("kendoDropDownList") || el.kendoDropDownList({
                                    autoBind: false,
                                    enable: false,
                                    dataTextField: "name",
                                    dataValueField: "id",
                                    optionLabel: "--Выберите город--",
                                    dataSource: new kendo.data.DataSource({
                                        schema: {
                                            model: areaModel.schema
                                        },
                                        change: function(e) {
                                            autoChangeAfterLoad.call(cities, that, e.items);
                                        }
                                    }),
                                    cascade: function(e) {
                                        
                                        if(citySelected > 0) {
                                            //deselectArea(that, citySelected);
                                        }
                                        
                                        citySelected = parseInt(this.value());
                                        
                                        
                                        if(citySelected > 0) {
                                            selectedArea(that, this.dataSource.get(citySelected));
                                        }
                                    }
                                }).data("kendoDropDownList");
                            }
                        },
                        {
                            el: $('<input type="text" />'),
                            callback: function(el) {
                                var that = this;
                                regions = el.data("kendoDropDownList") || el.kendoDropDownList({
                                    autoBind: false,
                                    enable: false,
                                    dataTextField: "name",
                                    dataValueField: "id",
                                    optionLabel: "--Выберите регион--",
                                    dataSource: new kendo.data.DataSource({
                                        schema: {
                                            model: areaModel.schema
                                        },
                                        change: function(e) {
                                            autoChangeAfterLoad.call(regions, that, e.items);
                                        }
                                    }),
                                    cascade: function(e) {
                                        var ds = cities.dataSource;
                                            
                                        if(citySelected > 0) {
                                            //deselectArea(cities, citySelected);
                                        }

                                        if(regionSelected > 0) {
                                            //deselectArea(that, regionSelected);
                                        }

                                        regionSelected = parseInt(this.value());
                                        ds.data([]);
                                        toggleTools(that, 1, false);

                                        if(regionSelected > 0) {
                                            selectedArea(that, this.dataSource.get(regionSelected));
                                            $.ajax(areaModel.url+'/'+regionSelected+'/?fields=lower', {
                                                cache: true,
                                                dataType: "json",
                                                contentType: "application/json",
                                                type: "GET"
                                            }).done(function(data) {
                                                if($.isArray(data.lower)) {
                                                    ds.data(data.lower);
                                                    toggleTools(that, 1, true);
                                                }
                                            });
                                        }
                                    }
                                }).data("kendoDropDownList");
                            }
                        },
                        {
                            el: $('<input type="text" id="countries" />'),
                            callback: function(el) {
                                var that = this;
                                countries = el.data("kendoDropDownList") || el.kendoDropDownList({
                                    dataTextField: "name",
                                    dataValueField: "id",
                                    optionLabel: "--Выберите страну--",
                                    dataSource: areaModel.getDataSource({
                                        pageSize: 300,
                                        filter: [{ field: "area_type", operator: "eq", value: "0" }],
                                        change: function(e) {
                                            autoChangeAfterLoad.call(countries, that, e.items);
                                        }
                                    }),
                                    cascade: function(e) {
                                        var rds = regions.dataSource,
                                            cds = cities.dataSource;
                                        
                                        
                                        if(citySelected > 0) {
                                            //deselectArea(cities, citySelected);
                                        }

                                        if(regionSelected > 0) {
                                            //deselectArea(regions, regionSelected);
                                        }
                                        
                                        if(countrySelected > 0) {
                                            //deselectArea(that, countrySelected);
                                        }
                                        
                                        countrySelected = parseInt(this.value());
                                        cds.data([]);
                                        rds.data([]);
                                        toggleTools(that, 0, false);

                                        if(countrySelected > 0) {
                                            selectedArea(that, this.dataSource.get(countrySelected));

                                            $.ajax(areaModel.url+'/'+countrySelected+'/?fields=lower', {
                                                cache: true,
                                                dataType: "json",
                                                contentType: "application/json",
                                                type: "GET"
                                            }).done(function(data) {
                                                if($.isArray(data.lower)) { 
                                                    rds.data(data.lower);
                                                    toggleTools(that, 0, true);
                                                }
                                            });
                                        } else {
                                        }
                                    }
                                }).data("kendoDropDownList");
                            }
                        }
                    ]
                    /*multiple: true
                    pivotFilter: {'vicinity': [false, 0]},
                    pivotList: {
                        'vicinity': { visible: false},
                        'distance': { visible: false}
                    },
                    onModelChange: function(modelData, field, data, multiple) {
                        relationChange(false, modelData, field, data);
                    }*/
                }).data("kendoRelations");
         content.find('[name="for_all"]').eq(0).bind("click", function () {
            if(this.checked){                
                deselectArea();
                
                content.find('#geolnk').hide();
            }else{
                content.find('#geolnk').show();
            }
        });
        var tmpX =  content.find('[name="for_all"]').eq(0);
        var check = function(tmp){
            if( tmp[0].checked){
                content.find('#geolnk').hide();
                
            }else{
                content.find('#geolnk').show();
            }
        };
        check(tmpX);
         
        if(dataModel.banner != undefined){
            if(dataModel.banner.path != undefined){  
                if(dataModel.banner.path.indexOf('.swf') + 1){
                    content.find('.imageContainer').html('<div id="flash'+dataModel.banner.id+'"></div>');
                    swfobject.embedSWF(dataModel.banner.path, 'flash'+dataModel.banner.id, dataModel.banner.width, dataModel.banner.height, "8.0.0");
                    content.find('.imageContainer').css({"padding-top":"10px"});
                    content.find('#flash'+dataModel.banner.id.toString()).css({"display": "block","margin": "0 auto"});
                }else{
                    content.find('.imageContainer').eq(0)
                            .css({"padding-top":"15px"})
                            .css({"padding-bottom":"15px"})
                            .css('backgroundImage', 'url('+dataModel.banner.path+')')
                            .css('height', parseInt(dataModel.banner.height)+30)
                            .css('width', '100%')
                            .css({"background-size": ""+dataModel.banner.width+" "+dataModel.banner.height+"",
                                  "background-position": " center center",
                                  "background-repeat": "no-repeat"})
                            .attr('id', 'image'+dataModel.banner.id);
                    //html('<img src="'+ dataModel.banner.path +'"/>');
                }
            }
        }
            var LOCALE = {
                cancel: "Отмена",
                dropFilesHere: "Перетащите файлы сюда",
                headerStatusUploaded: "Загружены",
                headerStatusUploading: "Загружаются...",
                remove: "Удалить",
                retry: "Попробовать ещё раз",
                select: "Выберите",
                statusFailed: "Ошибка загрузки",
                statusUploading: "Загружаются",
                uploadSelectedFiles: "Выберите файл"
            };
            var thisModel = cultturist.getModel('iblock');
            var onUploadSuccess = function() {
                var that = this;
            
                return function(e) {
                    var response = e.response,
                        xmodel = model,
                        xmodelField = 'banner_id',
                        xmodelData = dataModel;
            
                if (e.operation === 'remove') {
                	that.options.container.find('.imageContainer').eq(0).find('#image'+response.id).remove();
                } else if (e.operation === 'upload' && response.id > 0) {
                	var fileLabel = e.sender.wrapper.find('.k-filename[title="' + response['name'] + '"]');
                	fileEntry = fileLabel.closest(".k-file");

                	var filesData = fileEntry.data('files');
                	if (filesData !== undefined && filesData !== null) {
                    	filesData[0].name = xmodelData.id;
                    	fileEntry.data('files', filesData);
                	}

                	$.ajax('api/iblockbanner'+'/'+response.id+'/', {
                        cache: true,
                        dataType: "json",
                        contentType: "application/json",
                        type: "GET"
                    }).done(function(data) {
                        /*if (typeof data === 'object') {
                        	//that._buildImageView(data);

                        	if (that.options.multiple === true) {
                        		var aData = xmodelData.get(xmodelField);
                        		if (aData.files !== undefined) {
                        			aData = new Array(aData);
                        		}
                        		aData.push(data);
                        		for (var i = 0; i < aData.length; i++) {
                        			delete aData[i].files;
                        		}

                        		xmodelData.set(xmodelField, aData);
                        	} else {*/
                                xmodelData.set(xmodelField, response.id+"");
                        	//}

                        //}
                    });
                }
            };
        };
            
            var status = content.find('input[name=status]').eq(0);
            status.kendoDropDownList({
                autoBind: false,
                dataSource: {
                    data: this.options.status
                }
            });
            
            var position = content.find('input[name=position]').eq(0);
            position.kendoDropDownList({
                autoBind: false,
                dataSource: {
                    data: this.options.position
                }
            });
            content.find('#roleAdvImage')
            /*.eq(0).kendoCustomUpload({
                model: cultturist.getModel('iblockbanner'),
                modelField: 'banner',
                modelData: dataModel
            });*/
            .eq(0).kendoUpload({
                multiple: false,
                async: {
                        saveUrl: "api/iblockbanner",
                        removeUrl: "remove",
                        autoUpload: true
                },
                success: onUploadSuccess(thisModel)
            });
        }
    });
})(jQuery, window.cultturist);