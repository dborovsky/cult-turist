kendo_module({
    id: "customUpload",
    name: "Custom Upload",
    category: "web",
    description: "",
    depends: [ "core", "pager", "listview"]
});

(function($, undefined) {
    var kendo = window.kendo,
        Widget = kendo.ui.Widget,
        isFunction = kendo.isFunction,
        extend = $.extend,
        proxy = $.proxy,
        each = $.each,
        DOCUMENT = $(document),
        isLocalUrl = kendo.isLocalUrl,
        OPEN = "open",
        CLOSE = "close",
        LOCALE = {
            cancel: "Отмена",
            dropFilesHere: "Перетащите файлы сюда",
            headerStatusUploaded: "Загружены",
            headerStatusUploading: "Загружаются...",
            remove: "Удалить",
            retry: "Попробовать ещё раз",
            select: "Выберите",
            statusFailed: "Ошибка загрузки",
            statusUploading: "Загружаются",
            uploadSelectedFiles: "Выберите файл"
        },
        HTMLTEMPLATE = '<div class="customUpload">'+
                '<div class="imageContainer"></div>'+
                '<div class="uploadplace"><input type="file" /></div>'+
                '<br class="clear" />'+
            '</div>',
        NS = ".CustomUpload";

    var relations = Widget.extend({
        init: function(element, options) {
            var that = this;
            
            Widget.fn.init.call(that, element, options);
            
            that._buildContainer();
            
            if (that.options.multiple === true && options.modelData[options.modelField]) {
            	var allFiles = [];
            	$.each(options.modelData[options.modelField], function(ind, aFile) {
            		if (parseInt(aFile['status']) === 1) {
                		allFiles.push(aFile['name']);
                		that.options.upload._module.upload._enqueueFile(aFile['name'], {fileNames: [aFile['name']]});

            			var fileEl = that.options.upload._module.upload.wrapper.find('.k-filename[title="' + aFile['name'] + '"]').parent();
            			fileEl.addClass('k-file-success');
                		that.options.upload._module.upload._fileAction(fileEl, 'remove');

                		var filesPrepare = {};
                		filesPrepare.name = aFile['id'];
                		filesPrepare.status = aFile['status'];
                        fileEl.data('files', [filesPrepare]);
                        fileEl.data('fileNames', [filesPrepare]);
//            		} else {
//            			fileEl.addClass('k-file-error')
//            			fileEl.find('.k-upload-status').text('Файл удален');
            		}

            	});

            }

            that._buildImageView();
        },
        
        options: {
            name: "CustomUpload",
            model: false,
            modelField: false,
            modelData: false,
            container: false,
            dataSource: false,
            upload: false
        },
        events: [],
        _buildContainer: function() {
            var that = this,
                model = that.options.model,
                removeUrl = model.url;

            	if (model.options !== undefined && model.options.removeUrl !== undefined) {
                	removeUrl = model.options.removeUrl;
                }

            	removeVerb = 'POST';
            	if (model.options !== undefined && model.options.removeVerb !== undefined) {
            		removeVerb = model.options.removeVerb;
                }

            	removeField = 'fileNames';
            	if (model.options !== undefined && model.options.removeField !== undefined) {
            		removeField = model.options.removeField;
                }

            	var options = $.extend({}, {
                            multiple: false,
                            async: {
                                saveUrl: model.url,
                                removeUrl: removeUrl,
                                removeVerb: removeVerb,
                                removeField: removeField,
                                saveField: 'files'
                            },
                            localization: LOCALE,
                            success: that._onUploadSuccess(),
                            progress: that._onProgress(),
                        });
            if (that.options.multiple === true) {
            	options.multiple = true;
            }

            that.options.container = $(HTMLTEMPLATE).appendTo(that.element);
            that.options.upload = that.options.container.find('input[type=file]').eq(0).kendoUpload(options).data('kendoUpload');
        },
        _buildImageView: function(data) {
            var that = this,
                modelField = that.options.modelField,
                modelData = that.options.modelData,
                imageContainer = that.options.container.find('.imageContainer').eq(0);

            data = data || modelData[modelField];

            var showUploaded = function(data, multiple) {
                var files = data.files,
                selected = false;

                for (var index in files) {
	                if (!selected) {
	                    selected = files[index];
	                    continue;
	                }

	                var height = parseInt(files[index].height);
	                var width = parseInt(files[index].width);
	                if ((width > parseInt(selected.width) || height > parseInt(selected.height)) && height < 300 && width < 900) {
	                    selected = files[index];
	                }
	            }

	            if (selected) {
	                var ext = /.+\.(\w+)$/i.exec(selected.path);

	                if (ext && ext[1]) {
	                	if (multiple === true) {
	                		imageContainer.css('backgroundImage', 'none');
		                    if(/jpg|jpeg|png/i.test(ext[1])) {
		                    	if (parseInt(data.status) === 1) {
		                    		var subImageContainer = $('<div>').css('backgroundImage', 'url('+selected.path+')').css('height', selected.height).css('width', selected.width).attr('id', 'image'+data.id);
		                    		imageContainer.append(subImageContainer);
		                    	}
//		                    	if (parseInt(data.status) === 0) {
//		                    		subImageContainer.addClass('deleted');
//		                    	}

		                    } else {
		                    	if (parseInt(data.status) === 1) {
		                    		var subImageContainer = $('<div>').css('backgroundImage', 'url(/images/mimi/'+ext[1]+'-icon-128x128.png)').attr('id', 'image'+data.id);
		                    		imageContainer.append(subImageContainer);
		                    	}
//		                    	if (parseInt(data.status) === 0) {
//		                    		subImageContainer.addClass('deleted');
//		                    	}
		                    }
	                	} else {
		                    if(/jpg|jpeg|png/i.test(ext[1])) {
		                    	imageContainer.css('backgroundImage', 'url('+selected.path+')');
		                    } else {
		                    	imageContainer.css('backgroundImage', 'url(/images/mimi/'+ext[1]+'-icon-128x128.png)');
		                    }
	                	}
	                }
	                
	            }
            };

            if (that.options.multiple === true && data !== undefined && data.files === undefined) {
            	for (var i = 0; i < modelData[modelField].length; i++) {
            		showUploaded(modelData[modelField][i], true);
            	}
            } else if (typeof data === 'object') {
            	showUploaded(data, that.options.multiple);
            }
        },
        _onUploadSuccess: function() {
            var that = this;
            
            return function(e) {
                var response = e.response,
                    model = that.options.model,
                    modelField = that.options.modelField,
                    modelData = that.options.modelData;
            
                if (e.operation === 'remove') {
                	that.options.container.find('.imageContainer').eq(0).find('#image'+response.id).remove();
                } else if (e.operation === 'upload' && response.id > 0) {
                	var fileLabel = e.sender.wrapper.find('.k-filename[title="' + response['name'] + '"]');
                	fileEntry = fileLabel.closest(".k-file");

                	var filesData = fileEntry.data('files');
                	if (filesData !== undefined && filesData !== null) {
                    	filesData[0].name = response.id;
                    	fileEntry.data('files', filesData);
                	}

                	$.ajax(model.url+'/'+response.id+'/?fields=files', {
                        cache: true,
                        dataType: "json",
                        contentType: "application/json",
                        type: "GET"
                    }).done(function(data) {
                        if (typeof data === 'object') {
                        	that._buildImageView(data);

                        	if (that.options.multiple === true) {
                        		var aData = modelData.get(modelField);
                        		if (aData.files !== undefined) {
                        			aData = new Array(aData);
                        		}
                        		aData.push(data);
                        		for (var i = 0; i < aData.length; i++) {
                        			delete aData[i].files;
                        		}

                        		modelData.set(modelField, aData);
                        	} else {
                                modelData.set(modelField, response.id+"");
                        	}

                        }
                    });
                }
            };
        },
        _onProgress: function() {            
            var that = this;
            
            return function(e) {
                var files = e.files,
                    imageContainer = that.options.container.find('.imageContainer').eq(0);
                
                if(files[0] && files[0].extension) {
                    var ext = files[0].extension.substring(1);
                    imageContainer.css('backgroundImage', 'url(/images/mimi/'+ext+'-icon-128x128.png)');
                }
            };
        },
        destroy: function() {
            if(this.options.upload) {
                this.options.upload.destroy();
                this.options.upload = false;
            }
            
            this.element.off(NS);
            Widget.fn.destroy.call(this);
        }
    });

    kendo.ui.plugin(relations);

})(window.kendo.jQuery);
