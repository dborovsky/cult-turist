/**
 * FileGroup Model
 **/
(function($, cultturist) {
    cultturist.addModel('filegroup', {
        image: 'files',
        name: 'name',
        fields: {
            id: { type: "number"},
            name: { type: "string", validation: { required: true}},
            resource_type: { type: "number"},
            files: { type: "values", multiple: true},
            created_at: { type: "date", untouchable: false},
            updated_at: { type: "date", untouchable: false}
        },
        columns: [
            { field: "id", title: "ID", width:60},
            { field: "name", title: "Название", transport: 'name.ru', sortable: false},
            { field: "resource_type", title: "Тип", width:80},
            { field: "files", title: "Текст", atype: 'file', transport: 'files', filterable: false, sortable: false, hidden:true},
            { field: "created_at", title: "Создан", format: "{0:dd.MM.yyyy HH:mm:ss}", hidden:true},
            { field: "updated_at", title: "Обновлён", format: "{0:dd.MM.yyyy HH:mm:ss}", hidden:true},
            { command: [{ name: "destroy", text: "Удалить" }], width: 120}
        ],
        templates: {
            area_type: function (value, model) {
                var result = '';
                $.each(model.options.resourceType, function(index, item) {
                    if(item.id == value) {
                        result = item.name;
                    }
                });
                return result;
            }
        },
        filterables: {
            area_type: function(element, model) {
                element.kendoDropDownList({
                    dataTextField: "name",
                    dataValueField: "id",
                    dataSource: {
                        data: model.options.resourceType
                    },
                    optionLabel: "--Выберите значение--"
                });
            }
        },
        options:{
            resourceType : [
                { name: "Неустановлен", id: 0 },
                { name: "Страна(Регион, Город), ", id: 1 },
                { name: "Достопримечательность", id: 2 }
            ]
        }
    });

    cultturist.addModel('userfilegroup', {
        image: 'files',
        name: 'userfilegroupname',
        options : {
        	removeUrl: '/api/userfilegroup',
        	removeVerb: 'DELETE',
        	removeField: 'id'
        },
        fields: {
            id: { type: "number"},
            user_id: { type: "number"},
            name: { type: "string", validation: { required: true}},
            status: { type: "number"},
            choice: { type: "number"},
            admin_file: { type: "number", defaultValue: 1},
            files: { type: "values", multiple: true},
            created_at: { type: "date", untouchable: false},
            updated_at: { type: "date", untouchable: false}
        },
        columns: [
            { field: "id", title: "ID", width:60},
            { field: "name", title: "Название", transport: 'name.ru', sortable: false},
            { field: "user_id", title: "Пользователь"},
            { field: "admin_file", title: "Выбор администратора"},
            { field: "status", title: "Статус"},
            { field: "choice", title: "Выбор"},
            { field: "files", title: "Текст", atype: 'file', transport: 'files', filterable: false, sortable: false, hidden:true},
            { field: "created_at", title: "Создан", format: "{0:dd.MM.yyyy HH:mm:ss}", hidden:true},
            { field: "updated_at", title: "Обновлён", format: "{0:dd.MM.yyyy HH:mm:ss}", hidden:true},
            { command: [{ name: "destroy", text: "Удалить" }], width: 120}
        ]
    });
})(jQuery, window.cultturist);

function standartAreFiltering(element, model) {
    var countriesInput = $('<input type="text" id="countries" />'),
        regionsInput = $('<input type="text" />'),
        citiesInput = $('<input type="text" />'),
        areaModel = cultturist.getModel('area'),
        countrySelected = 0,
        regionSelected = 0,
        citySelected = 0;

    element.after(citiesInput)
            .after(regionsInput)
            .after(countriesInput).attr('type', 'hidden');

    var countries = countriesInput.kendoDropDownList({
        dataTextField: "name",
        dataValueField: "id",
        optionLabel: "--Выберите страну--",
        dataSource: areaModel.getDataSource({
            pageSize: 300,
            filter: [{ field: "area_type", operator: "eq", value: "0" }]
        }),
        cascade: function(e) {
            var rds = regions.dataSource,
                cds = cities.dataSource;

            countrySelected = parseInt(this.value());
            cds.data([]);
            rds.data([]);
            cities.enable(false);
            regions.enable(false);

            if(countrySelected > 0) {

                element.val(countrySelected);

                $.ajax(areaModel.url+'/'+countrySelected+'/?fields=lower', {
                    cache: true,
                    dataType: "json",
                    contentType: "application/json",
                    type: "GET"
                }).done(function(data) {
                    if($.isArray(data.lower)) { 
                        rds.data(data.lower);
                        regions.enable(true);
                    }
                });
            } else {
                element.val("");
            }
            element.trigger("change");
        }
    }).data("kendoDropDownList");

    var regions = regionsInput.kendoDropDownList({
        autoBind: false,
        enable: false,
        dataTextField: "name",
        dataValueField: "id",
        optionLabel: "--Выберите регион--",
        dataSource: new kendo.data.DataSource({
            schema: {
                model: areaModel.schema
            }
        }),
        cascade: function(e) {
            var ds = cities.dataSource;

                regionSelected = parseInt(this.value());
                ds.data([]);
                cities.enable(false);

            if(regionSelected > 0) {
                element.val(regionSelected);
                $.ajax(areaModel.url+'/'+regionSelected+'/?fields=lower', {
                    cache: true,
                    dataType: "json",
                    contentType: "application/json",
                    type: "GET"
                }).done(function(data) {
                    if($.isArray(data.lower)) {
                        ds.data(data.lower);
                        cities.enable(true);
                    }
                });
            } else {
                element.val(countrySelected || "");
            }

            element.trigger("change");
        }
    }).data("kendoDropDownList");

    var cities = citiesInput.kendoDropDownList({
        autoBind: false,
        enable: false,
        dataTextField: "name",
        dataValueField: "id",
        optionLabel: "--Выберите город--",
        dataSource: new kendo.data.DataSource({
            schema: {
                model: areaModel.schema
            }
        }),
        cascade: function(e) {
            citySelected = parseInt(this.value());

            if(citySelected > 0) {
                element.val(citySelected);
            } else {
                element.val(regionSelected || countrySelected || "");
            }

            element.trigger("change");
        }
    }).data("kendoDropDownList");
};


if (kendo.ui.Editor) {
    function onCustomTextWrap (e) {
    	var editor = $(this).data("kendoEditor");
    	var range = editor.getRange();

    	var images = [],
    		texts = [],
    		others = [],
    		wrapClass = 'custom_wrap_left',
    		imageIndex = -1,
    		curIndex = 0;

    	var checkType = function (node) {
    		if (kendo.ui.editor.Dom.is(node, 'img')) {
                images.push(node);
                wrapClass = 'custom_wrap_right';
                imageIndex = curIndex;
                curIndex++;
            } else if (kendo.ui.editor.Dom.is(node, '#text')) {
            	if (node.wholeText.trim().length > 0) {
            		texts.push(node);
                	wrapClass = 'custom_wrap_left';            			
            		curIndex++;
            	}
            } else if (!kendo.ui.editor.Dom.is(node, 'br')) {
            	others.push(node);
            }
    	}

    	new kendo.ui.editor.RangeIterator(range).traverse(function (node) {
    		if (kendo.ui.editor.Dom.is(node, 'p')) {
    			var paragraphChildren = kendo.ui.editor.Dom.significantChildNodes(node);
    			$.each(paragraphChildren, function(key, val) {
    				checkType(val);
    			});
    		} else {
    			checkType(node);
    		}
    	});

    	if (texts.length === 0 || images.length !== 1 || others.length > 0) {
    		alert("Необходимо выбрать одно изображение и текст (теги p).");
    	} else if (imageIndex === 0 || imageIndex === (curIndex - 1)) {
    		var nodes = kendo.ui.editor.RangeUtils.nodes(range);
    		var doc = kendo.ui.editor.RangeUtils.documentFromRange(range);
    		var container = kendo.ui.editor.Dom.create(window.document, 'div', {'className' : wrapClass});

    		var imageContainer = kendo.ui.editor.Dom.create(window.document, 'div', {'className' : 'imageContainer'});
    		imageContainer.appendChild(images[0]);
    		container.appendChild(imageContainer);
    		for (var i = 0; i < texts.length; i++) {
    			var text = kendo.ui.editor.Dom.create(doc, 'p');
    			text.appendChild(texts[i]);
    			container.appendChild(text);
    		}

    		range.deleteContents();
    		range.insertNode(container);
        	editor.selectRange(range);
		} else {
			alert("Изображение должно быть слева или справа.");
		} 
    }

    kendo.ui.Editor.prototype.options.stylesheets = [
        '/css/admin.css'
    ],
    kendo.ui.Editor.prototype.options.tools = [
        "bold",
        "italic",
        "underline",
        "strikethrough",
        "justifyLeft",
        "justifyCenter",
        "justifyRight",
        "justifyFull",
        "insertUnorderedList",
        "insertOrderedList",
        "indent",
        "outdent",
        "createLink",
        "unlink",
        "insertImage",
        "subscript",
        "superscript",
        "createTable",
        "addRowAbove",
        "addRowBelow",
        "addColumnLeft",
        "addColumnRight",
        "deleteRow",
        "deleteColumn",
        "viewHtml",
        "formatting",
        "cleanFormatting",
        "fontName",
        "fontSize",
        "foreColor",
        "backColor",
        {
        	name: 'custom_text_wrap',
        	tooltip: 'Сделать изображение обтекаемое текстом',
        	exec: onCustomTextWrap
        }
    ];

    kendo.ui.Editor.prototype.options.imageBrowser = {
        transport: {
             read: function (options) {
            	 if (options.data.sort !== undefined) {
            		 var order = '';
        			 $.each(options.data.sort, function(key, val) {
        				 if (order.length > 0) {
        					 order += ',';
        				 }

        				 if (val.field === 'date_asc') {
        					 order += 'created_at:asc';
        				 } else if (val.field === 'date_desc') {
        					 order += 'created_at:desc';
        				 }
        			 });
            		 delete options.data.sort;
            		 options.data.order = order;
            	 }

            	 if (options.data.pageSize !== undefined && options.data.pageSize !== '') {
            		 options.data.pagesize = options.data.pageSize;
            	 } else {
            		 delete options.data.pageSize;
            	 }

            	 if (options.data.filter !== undefined && options.data.filter.filters[0].value.length > 2) {
            		 options.data.filter = options.data.filter.filters[0].field + ':like:%' + options.data.filter.filters[0].value+'%';
            	 } else {
            		 delete options.data.filter;
            	 }

            	 options.url = '/api/userfilegroup/';
            	 kendo.data.RemoteTransport.fn['read'].call(this, options);
             },
             thumbnailUrl: function(path, file) {
                 return path + file;
             },
             imageUrl: function(path) {
            	 return path;
             }
        },
        schema: {
        	total: 'total',
            data: function(data) {
                return data.results || data || [];
            }
        }
    };


    var ImageBrowser = kendo.ui.ImageBrowser.extend({
    	init: function(element, options) {
            var that = this;

            options = options || {};

            kendo.ui.Widget.fn.init.call(that, element, options);

            that.element.addClass("k-imagebrowser k-secondary");

            that.element
                .on('click.kendoButton', ".k-toolbar button:not(.k-state-disabled):has(.k-delete)", $.proxy(that._deleteClick, that))
                .on('click.kendoButton', ".k-toolbar button:not(.k-state-disabled):has(.k-addfolder)", $.proxy(that._addClick, that))
                .on("keydown.kendoButton", "li.k-state-selected input", $.proxy(that._directoryKeyDown, that))
                .on("blur.kendoButton", "li.k-state-selected input", $.proxy(that._directoryBlur, that));

            that._dataSource();

            that.refresh();

            that.path(that.options.path);

            $("#imagePager").kendoPager({
                autoBind: true,
                input: true,
                numeric: false,
                pageSizes: [20, 50, 100, 300, 500],
                refresh: true,
                dataSource: that.dataSource
            });
        },
    	_loadImage: function(li) {
	        var that = this,
	        element = $(li),
	        dataItem = that.dataSource.getByUid(element.attr(kendo.attr("uid"))),
	        name = dataItem.get('name'),
	        thumbnailUrl = that.options.transport.thumbnailUrl,
	        img = $("<img />", { alt: name }),
	        urlJoin = "?";
		
		    img.hide()
		       .on("load" + NS, function() {
		           $(this).prev().remove().end().addClass("k-image").fadeIn();
		       });
		
		    element.find(".k-loading").after(img);
		
		    if (kendo.isFunction(thumbnailUrl)) {
		        thumbnailUrl = thumbnailUrl(dataItem.files[0]['path'], '');
		    } else {
		        if (thumbnailUrl.indexOf("?") >= 0) {
		            urlJoin = "&";
		        }
		
		        thumbnailUrl = thumbnailUrl + urlJoin + "path=" + that.path() + encodeURIComponent(name);
		    }
		
		    // IE8 will trigger the load event immediately when the src is assigned
		    // if the image is loaded from the cache
		    img.attr("src", thumbnailUrl);
		
		    li.loaded = true;
    	},
        value: function() {
            var that = this,
                selected = that._selectedItem(),
                path,
                imageUrl = that.options.transport.imageUrl;

            if (selected && selected.get('type') === "f") {
            	var html = '<div>';
                $.each(selected.get('files'), function (key, file) {
                	var preset = 'Standard';
                	var exploded = file['path'].split('/');
                	if (exploded.length > 1) {
                		preset = exploded[exploded.length - 2];
                	}

                	html += '<div class="presetSelect"><label for="type_'+key+'">'+preset+'</label><button type="button" name="type_'+key+'" value="'+key+'">' + file['width'] + 'x' + file['height'] + '</button></div>';
                });
                html += '</div>';

                var kendoWindow = $("<div />").kendoWindow({
                    title: "Выберите размер",
                    resizable: false,
                    width: "200px",
                    modal: true
                });

                kendoWindow.data("kendoWindow").content(html).center().open();

                kendoWindow.find('button').click(function () {
                	kendoWindow.data("kendoWindow").close()
                	$("#k-editor-image-url").val(selected['files'][this.value]['path']);
                });

                return '';
            }
        },
        _dataSource: function() {
            var that = this,
                options = that.options,
                transport = options.transport,
                schema,
                dataSource = {
            		serverSorting: true,
            		serverFiltering: true,
            		serverPaging: true,
            		pageSize: 50,
                    type: transport.type || "imagebrowser",
                    sort: [{ field: 'date_desc', dir: "desc" }]
                };

            if ($.isPlainObject(transport)) {
                transport.path = $.proxy(that.path, that);
                dataSource.transport = transport;
            }

            if ($.isPlainObject(options.schema)) {
                dataSource.schema = options.schema;
            } else if (transport.type && $.isPlainObject(kendo.data.schemas[transport.type])) {
                schema = kendo.data.schemas[transport.type];
            }

            if (that.dataSource && that._errorHandler) {
                that.dataSource.unbind(ERROR, that._errorHandler);
            } else {
                that._errorHandler = $.proxy(that._error, that);
            }

            that.dataSource = kendo.data.DataSource.create(dataSource)
                .bind('error', that._errorHandler);
        },
        _toolbar: function() {
        	TOOLBARTMPL = '<div class="k-widget k-toolbar k-header k-floatwrap">' +
	            '<div class="k-toolbar-wrap">' +
	                '# if (showUpload) { # ' +
	                    '<div class="k-widget k-upload"><div class="k-button k-button-icontext k-upload-button">' +
	                        '<span class="k-icon k-add"></span>#=messages.uploadFile#<input type="file" name="file" /></div></div>' +
	                '# } #' +
	
	                '# if (showCreate) { #' +
	                     '<button type="button" class="k-button k-button-icon"><span class="k-icon k-addfolder" /></button>' +
	                '# } #' +
	
	                '# if (showDelete) { #' +
	                    '<button type="button" class="k-button k-button-icon k-state-disabled"><span class="k-icon k-delete" /></button>&nbsp;' +
	                '# } #' +
	            '</div>' +
	            '<div class="k-tiles-arrange">' +
	                '<label>Сортировка: <select /></label></a>' +
	            '</div>' +
	            '<div id="imagePager"></div>' +
            '</div>';
            var that = this,
                template = kendo.template(TOOLBARTMPL),
                messages = that.options.messages,
                arrangeBy = [
                    { text: 'Дата по убыванию', value: "date_desc" },
                    { text: 'Дата по возрастанию', value: "date_asc" }
                ],
                pageSize = [
                     { text: '20', value: "20" },
                     { text: '50', value: "50" },
                     { text: '100', value: "100" },
                     { text: '300', value: "300" },
                     { text: '500', value: "500" },
                     { text: '1000', value: "1000" }
                ],
                page = [{ text: '1', value: '1' }];

            that.toolbar = $(template({
                    messages: messages,
                    showUpload: that.options.transport.uploadUrl,
                    showCreate: that.options.transport.create,
                    showDelete: that.options.transport.destroy
                }))
                .appendTo(that.element)
                .find(".k-upload input")
                .kendoUpload({
                    multiple: false,
                    localization: {
                        dropFilesHere: messages.dropFilesHere
                    },
                    async: {
                        saveUrl: that.options.transport.uploadUrl,
                        autoUpload: true
                    },
                    upload: $.proxy(that._fileUpload, that),
                    error: function(e) {
                        that._error({ xhr: e.XMLHttpRequest, status: "error" });
                    }
                }).end();

            that.upload = that.toolbar
                .find(".k-upload input")
                .data("kendoUpload");

            that.arrangeBy = that.toolbar.find(".k-tiles-arrange select")
                .kendoDropDownList({
                    dataSource: arrangeBy,
                    dataTextField: "text",
                    dataValueField: "value",
                    change: function() {
                        that.orderBy(this.value());
                    }
                })
                .data("kendoDropDownList");

            that._attachDropzoneEvents();
        }
    });
    
    kendo.ui.plugin(ImageBrowser);
}