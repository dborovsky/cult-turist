<?php
namespace v1;

use App\Controller\AbstractController,
    App\Entity\FeatureEntity,
    App\Entity\LandmarkEntity,
    App\Entity\AreaEntity,
    App\Application;

class SeeAlso extends AbstractController
{
    
    public function index($entity_name = '', $entity_id = '', $fields = '', $page = null, $pageSize = null)
    {
        header('Access-Control-Allow-Origin: *');
        $pageSize = intval($pageSize) > 0 ? intval($pageSize) : 3;

        $valid_entities = ['feature', 'people', 'kitchen', 'event'];
        // filter
        $index = array_search($entity_name, $valid_entities);
        $entity_name_f = $valid_entities[$index];
        $entity_class = 'App\Entity\\' . ucfirst($entity_name_f) . 'Entity';

        if ($index !== false) {
            $featureCm = static::createConditionsMap('v1\\'.ucfirst($entity_name_f),
                'seealso,seealso.area,seealso.area.name,seealso.web_image,seealso.web_image.files,area.upper,area.upper.upper',
                $entity_name_f.'.id:eq:'.$entity_id);
            $curFeature = static::getSingle((new $entity_class())->newQuery(), $featureCm, $entity_id);

            $data = $curFeature['seealso'];

            if (count($data) < $pageSize) {
                // get current and parent areas
                $currentAreas = [];
                $parentAreas = [];
                foreach ($curFeature['area'] as $curArea) {
                    if ((int)$curArea['system'] === 0) {
                        $currentAreas[$curArea['area_type']][$curArea['id']] = $curArea['name'];
                        foreach ($curArea['upper'] as $upperArea) {
                            $parentAreas[$upperArea['area_type']][$upperArea['id']]= $upperArea['alias'];
                        }
                    }
                }

                $parentLevel = 0;
                if (empty($parentAreas) === false) {
                    $parentLevel = max(array_keys($parentAreas));
                } else {
                    $parentAreas = $currentAreas;
                }

                if (empty($currentAreas) === false) {
                    $this->_getData($entity_name_f, $data, $parentLevel, $parentAreas, $currentAreas, $fields, $pageSize, $entity_id);
                }

                if (count($data) < $pageSize && empty($currentAreas) === false) {
                    $parentLevel--;
                    $this->_getData($entity_name_f, $data, $parentLevel, $parentAreas, $currentAreas, $fields, $pageSize, $entity_id);
                }
            }

            foreach ($data as &$item) {
                foreach ($item['parent_area'] as $key => $val) {
                    if ((int)$val['system'] === 1) {
                        unset($item['parent_area'][$key]);
                    }
                }
            }

            // todo: refactor this piece of shit
            foreach ($data as &$entity) {
                $entity = MaybeInteresting::flattenAreas($entity);
            }
            
            return ['data' => $data];
        }
        else if ($entity_name === 'landmark')
        {
            $landmarkCm = static::createConditionsMap('v1\Landmark', 'id,name,area', 'landmark.id:eq:'.$entity_id);
            $curFeature = static::getSingle((new LandmarkEntity())->newQuery(), $landmarkCm, $entity_id);

            $nearestTmp = $this->_getNearestLandmark($curFeature['latitude'], $curFeature['longitude']);
            $nearest = [];
            $counter = 0;
            foreach ($nearestTmp as $landmark) {
                if ((int)$landmark['id'] !== (int)$entity_id && $landmark['distance'] <= 150000) {
                    $nearest[$landmark['id']]= $landmark['distance'];
                    if (++$counter > 2) {
                        break;
                    }
                }
            }

            $landmarkCm2 = static::createConditionsMap('v1\Landmark', 'id,name,image.files,area,area.upper,area.upper.name,area.upper.upper,area.upper.upper.name', 'id:in:'.implode(',', array_keys($nearest)));
            $data = static::getCollection((new LandmarkEntity())->newQuery(), $landmarkCm2, null, count($nearest));
            $data = array_reverse($data);
            foreach ($data as &$item) {
                $item['distance'] = round($nearest[$item['id']]*1000, -2);
                $minArea = null;
                foreach ($item['area'] as $area) {
                    if ($minArea === null || ((int)$area['system'] === 0 && (int)$area['area_type'] > (int)$minArea['area_type'])) {
                        $minArea = $area;
                    }
                }

                $item['area'] = [$minArea];
                if (empty($minArea['upper']) === false) {
                    foreach ($minArea['upper'] as $upper) {
                        if ((int)$upper['area_type'] === (int)$minArea['area_type'] - 1) {
                            $item['area'][] = $upper;
                            if (empty($upper['upper']) === false) {
                                foreach ($upper['upper'] as $upperUpper) {
                                    if ((int)$upperUpper['area_type'] === (int)$upper['area_type'] - 1) {
                                        $item['area'][] = $upperUpper;
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }

                    $item['area'] = array_reverse($item['area']);
                }
            }

            return ['data' => $data];
        }
        
    }

    private function _getData($entity_name_f, &$data, $parentLevel, $parentAreas, $currentAreas, $fields, $pageSize, $entity_id) {
        $entity_class = 'App\Entity\\' . ucfirst($entity_name_f) . 'Entity';

        $areasCm = static::createConditionsMap('v1\Area', 'id,lower', 'id:in:'.implode(',', array_keys($parentAreas[$parentLevel])));
        $parents = static::getCollection((new AreaEntity())->newQuery(), $areasCm, 1, 100);

        $curAreasIds = [];
        if (isset($currentAreas[$parentLevel + 1]) === true) {
            $curAreasIds = array_keys($currentAreas[$parentLevel + 1]);
        }

        $seeAlsoAreas = [];
        // TODO: refactor this!
        // fetch all child areas except current
        foreach ($parents as $aParent) {
            foreach ($aParent['lower'] as $sibling) {
                if ((array_search($sibling['id'], $curAreasIds) === false)) {
                    $seeAlsoAreas []= $sibling['id'];
                }
            }
        }

        // TODO: refactor this! to large query
        $featureCm = static::createConditionsMap('v1\\'.ucfirst($entity_name_f),
            'id,name,parent_area,parent_area.parent_area.name,parent_area.parent_area.parent_area.name,web_image.files',
            'parent_area.id:in:'.implode(',', $seeAlsoAreas).';system:eq:0', '');
        $seeAlsoFeatures = static::getCollection((new $entity_class())->newQuery(), $featureCm, 1, 100);

        $counter = count($data);

        if (count($seeAlsoFeatures) > 0) {
            $maxRand = count($seeAlsoFeatures) - 1;
            $numbers = range(0, $maxRand);
            shuffle($numbers);

            $curCount = count($data);
            $randNums = array_slice($numbers, 0, $pageSize - $curCount + 1);

            foreach ($randNums as $rand) {
                if ((int)$seeAlsoFeatures[$rand]['id'] !== (int)$entity_id) {
                    $data []= $seeAlsoFeatures[$rand];
                    $counter++;
                }

                if ($counter >= $pageSize) {
                    break;
                }
            }
        }
    }

    private function _getNearestLandmark($latitude, $longitude) {
        $db = Application::getCapsule()->connection();

        if (!$db) {
            throw new RestException(500);
        }

        return $db->select($db->raw("select id, distonearth(latitude,longitude,".$latitude.','.$longitude.")/1000 as distance
            from landmark order by distance limit 5"));
    }
}

?>