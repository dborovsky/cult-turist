<?php
namespace v1;

use App\Controller\AbstractController,
    App\Entity\PeopleEntity,
    App\Entity\KitchenEntity,
    App\Entity\FeatureEntity,
    App\Entity\LandmarkEntity,
    App\Entity\AreaEntity,
    App\Application;

class MaybeInteresting extends AbstractController
{

    private $feature_default_id     = 2843;
    private $landmark_default_id    = 2764;
    private $people_default_id      = 650;
    private $kitchen_default_id     = 2666;
    private $area_default_id        = 333;

    /**
     * @param string $entity_name
     * @param string $entity_id
     * @param string $area_id
     * @param string $fields
     * @return array
     *
     * select and group random entities of all types except specified type ( TODO: need to refactor according api style! )
     */
    public function index($entity_name = 'area', $entity_id = '', $area_id = '', $fields = '', $page = null, $pageSize = null)
    {
        header('Access-Control-Allow-Origin: *');
        $allowable_entities = ['landmark', 'feature', 'people', 'kitchen', 'area'];
        // exclude current entity type
        $entities = array_diff($allowable_entities, (array)$entity_name);

        // ugly hack for events
        if($entity_name == 'event')
        {
            unset($entities[2]);
        }

        $data = [];

        $curArea = (new AreaEntity())->newQuery()
            ->with('parent_area', 'parent_area.name', 'parent_area.parent_area.name')
            ->findOrFail($area_id)
            ->toArray()
        ;
        $curArea = $this->flattenAreas($curArea);
        $curCountry = isset($curArea['area'][0])?$curArea['area'][0]:null;
        $curRegion = isset($curArea['area'][1])?$curArea['area'][1]:null;

        foreach ($entities as $one) {
            $one_entity_class = 'App\Entity\\' . ucfirst($one) . 'Entity';

            if($one == 'landmark') {

                $res = (new $one_entity_class())->newQuery()
                    ->joinWithAreaAncestors()
                        ->where(function ($query) use ($curArea) {
                            if($curArea['area_type']=='2') {
                                $query
                                    ->where('cities.id', $curArea['id']);
//                                    ->orWhere('regions.id', $curRegion['id']);
                            }
                            else {
                                $query
                                    ->where('regions.id', $curArea['id']);
                            }
                        })
                    ->with('name', 'parent_area.name', 'parent_area.parent_area.name',
                        'parent_area.parent_area.parent_area.name', 'web_image.files')
                    ->orderByRaw(($curArea['area_type']=='2'?'':'landmark.regional DESC, ').'RAND()')
                    ->limit(1)
//                    ->toSql()
                    ->first()
                ;

            } elseif($one == 'area') {
                // get random city in current region or current country
                $res = (new AreaEntity())->newQuery()
                    ->joinCitiesWithRegionsAndCountries()
                    ->where(function ($query) use ($curCountry, $curRegion) {
                        $query
                            ->where('regions.id', $curRegion['id'])
                            ->orWhere('countries.id', $curCountry['id']);
                    })
                    ->with('name', 'parent_area.name', 'parent_area.parent_area.name', 'web_image.files')
                    ->orderByRaw(($curRegion['id']?'regions.id='.$curRegion['id'].' DESC, ':'').'RAND()')
                    ->limit(1)
//                    ->toSql();
                    ->first();

            } else {
                // only for feature kitchen people
                $res = (new $one_entity_class())->newQuery()
                    ->with('name', 'prcountry.name', 'prregion.name', 'web_image.files')
                    ->whereHas('prcountry',function($query) use ($curCountry) {
                        $query->where('country_id', $curCountry['id']);
                    })
                    ->orderByRaw('RAND()')
                    ->limit(1)
                    ->first();

            }

            // if no result - get default entity
            if($res) {
                $data[] = array_merge(['entityName'=>$one], $res->toArray()) ;
            } else {
                $query = (new $one_entity_class())->newQuery();

                if($one == 'landmark')
                    $query->with('parent_area', 'name', 'parent_area.name',
                        'parent_area.parent_area.name', 'web_image.files', 'property.icon.files');
                elseif ($one == 'area')
                    $query->with('name', 'parent_area.name', 'web_image.files');
                else
                    $query->with('name', 'prcountry.name', 'prregion.name', 'web_image.files');

                $get_out_at_least = $query
                    ->find($this->{$one.'_default_id'})
                    ->toArray();
                $data[] = array_merge(['entityName'=>$one], $get_out_at_least);
            }
        }

        // flatten areas
        foreach ($data as &$entity) {
            $entity = self::flattenAreas($entity);
        }
//        $data = $this->retrieveDefinedColumn($data, 'name', 'ru');

        return ['data' => $data];
    }

    public static function flattenAreas($entity)
    {
        if(isset($entity['parent_area']) && count($entity['parent_area'])) {
            foreach ($entity['parent_area'] as $one){
                $entity['area'][$one['area_type']] = $one;
                $entity['area'][$one['area_type']] = self::retrieveDefinedColumn($entity['area'][$one['area_type']], 'name', 'ru');
                if(isset($one['parent_area']) && count($one['parent_area'])) {
                    foreach ($one['parent_area'] as $two){
                        $entity['area'][$two['area_type']] = $two;
                        $entity['area'][$two['area_type']] = self::retrieveDefinedColumn($entity['area'][$two['area_type']], 'name', 'ru');
                        if(isset($two['parent_area']) && count($two['parent_area'])) {
                            foreach ($two['parent_area'] as $three){
                                $entity['area'][$three['area_type']] = $three;
                                $entity['area'][$three['area_type']] = self::retrieveDefinedColumn($entity['area'][$three['area_type']], 'name', 'ru');
                            }
                        }
                    }
                }
            }
        }

        if(isset($entity['prcountry'])&&count($entity['prcountry'])) {
            $entity['area'][$entity['prcountry'][0]['area_type']] = $entity['prcountry'][0];
            $entity['area'][$entity['prcountry'][0]['area_type']]  = self::retrieveDefinedColumn($entity['area'][$entity['prcountry'][0]['area_type']] , 'name', 'ru');
        }
        if(isset($entity['prregion'])&&count($entity['prregion'])) {
            $entity['area'][$entity['prregion'][0]['area_type']] = $entity['prregion'][0];
            $entity['area'][$entity['prregion'][0]['area_type']]  = self::retrieveDefinedColumn($entity['area'][$entity['prregion'][0]['area_type']] , 'name', 'ru');
        }

        sort($entity['area']);
        return $entity;
    }

    public static function retrieveDefinedColumn($entity, $relation_name, $column_name)
    {
        if(is_array($entity[$relation_name]) && $entity[$relation_name][$column_name])
            $entity[$relation_name] = $entity[$relation_name][$column_name];
        return $entity;
    }
}