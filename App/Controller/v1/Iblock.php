<?php
namespace v1;

use App\Controller\AbstractController,
    App\Model\Mapper\BaseMapper;

class Iblock extends AbstractController
{    
    public function getConfig()
    {
        return [
            'entity' => 'App\Entity\IblockEntity',
            'primary' => 'id',
            'fields' => [
                'id' => [
                    'access' => BaseMapper::CONTROL_VISIBLE
                 ],
                'name' => [
                    'relation' => BaseMapper::RELATION_BELONGSTO,
                    'validator'=>'String',
                    'resource' => 'v1\SiteString',
                    'access' => BaseMapper::CONTROL_VISIBLE,
                    'inject' => 'ru',
                    'foreignkey' => 'name_id'
                 ],
                'partner_name' => [
                    'relation' => BaseMapper::RELATION_BELONGSTO,
                    'validator'=>'String',
                    'resource' => 'v1\SiteString',
                    'access' => BaseMapper::CONTROL_VISIBLE,
                    'inject' => 'ru',
                    'foreignkey' => 'partner_id'
                ],
                'banner_link' => [
                    'relation' => BaseMapper::RELATION_BELONGSTO,
                    'validator'=>'String',
                    'resource' => 'v1\SiteString',
                    'inject' => 'ru',
                    'foreignkey' => 'link_id'
                ],
                'position' => [
                    'validator'=>'String',
                ],
                'auto_off' => [
                    'validator'=>'Boolean',
                ],
                'for_all' => [
                    'validator'=>'Boolean',
                ],
                'area' => [
                    'relation' => BaseMapper::RELATION_BELONGSTOMANY,
                    'resource' => 'v1\Area',
                ],
                'area_flag' => [
                    'validator'=>'Boolean',
                ],
                'landmark' => [
                    'validator'=>'Boolean',
                ],
                'people' => [
                    'validator'=>'Boolean',
                ],
                'event' => [
                    'validator'=>'Boolean',
                ],
                'kitchen' => [
                    'validator'=>'Boolean',
                ],
                'feature' => [
                    'validator'=>'Boolean',
                ],
                'banner' => [
                    'relation' => BaseMapper::RELATION_BELONGSTO,
                    'validator'=>'File',
                    'resource' => 'v1\IblockBanner'
                ],
                'banner_id' => [
                    'access' => BaseMapper::CONTROL_VISIBLE
                ],
                'widget' => [
                    'relation' => BaseMapper::RELATION_BELONGSTO,
                    'validator'=>'String',
                    'resource' => 'v1\SiteString',
                    'inject' => 'ru',
                    'foreignkey' => 'widget_id'
                 ],
                'status' => [
                    'validator'=>'Constrain',
                    'options' => ['values' => [
                        0 => 'Недоступен',
                        1 => 'Доступен'
                    ]],
                    'access' => BaseMapper::CONTROL_VISIBLE,
                    'modefire' => 'App\Modefire\Constrain'
                 ],
                'date_begin' => [
                    'validator'=>'DateTime',
                    'modefire' => 'App\Model\Modefire\DateTime'
                 ],
                'date_end' => [
                    'validator'=>'DateTime',
                    'modefire' => 'App\Model\Modefire\DateTime'
                 ],
                'created_at' => [
                    'validator'=>'DateTime',
                    'modefire' => 'App\Model\Modefire\DateTime'
                ],
                'updated_at' => [
                    'validator'=>'DateTime',
                    'modefire' => 'App\Model\Modefire\DateTime'
                ]
            ]
        ];
    }
}