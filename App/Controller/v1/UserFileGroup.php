<?php
namespace v1;

use App\Application,
    App\Model\Utils\ImageUploadProcessor,
    App\Model\Mapper\ConditionsMap,
    App\Controller\AbstractController,
    App\Model\Mapper\BaseMapper,
    App\Model\Manager\UploadManager,
    Luracast\Restler\RestException;

class UserFileGroup extends AbstractController
{
    /**
     * smart-auto-routing false
     * @return array
     */
    public function post($data = null)
    {
        
        if(!class_exists($this->entity)) {
            throw new RestException(400, "'$this->entity' entity missing");
        }
        
        if(!isset($_FILES['files']) || !count($_FILES['files'])) {
            throw new RestException(400, "No files to upload");
        }
        $file = UploadManager::getInstance()->fileViaUpload('files', UploadManager::USER_FILE);
        //var_dump($file);
        if(!$file) {
            throw new RestException(500, 'File type unsupported!');
        }
        
        if($file->getOptions()['handling'] == 'image') {
            $collection = ImageUploadProcessor::createImagesBySizes($file, Application::getImageUploadSizes());
        } elseif($file->getOptions()['handling'] == 'maps') {
            $collection = MapFileUploadProcessor::createMapFile($file);
        } else {
            throw new RestException(500);
        }

        $result = UploadManager::getInstance()->getResult($collection, UploadManager::USER_FILE);

        if ((int)$result->user->id === 1) {
            $result->status = 1;
            $result->save();
        }

        $conditionsMap = new ConditionsMap();
        $conditionsMap->setIncludeHidden(true);
        $conditionsMap->setController(get_class($this));
        //print_r(Application::getSQLLog());
        var_dump(static::resultModefire($result->toArray(), $conditionsMap->getOptionsMap()));
        return static::resultModefire($result->toArray(), $conditionsMap->getOptionsMap());
    }
    
    /**
     * smart-auto-routing false
     * @param int $id {@from path}
     * @return array
     */
    public function put($id, $data = null)
    {
        if(intval($id) < 1) {
            throw new RestException(400, "Missing 'id' param!");
        }
        
        if(!class_exists($this->entity)) {
            throw new RestException(400, "'$this->entity' entity missing");
        }
        
        if(!isset($_FILES['files']) || !count($_FILES['files'])) {
            throw new RestException(400, "No files to upload");
        }
        
        $file = UploadManager::getInstance()->fileViaUpload('files', UploadManager::USER_FILE);
        
        if(!$file) {
            throw new RestException(500, 'File type unsupported!');
        }
        
        if($file->getOptions()['handling'] == 'images') {
            $collection = ImageUploadProcessor::createImagesBySizes($file, Application::getImageUploadSizes());
        } elseif($file->getOptions()['handling'] == 'maps') {
            $collection = MapFileUploadProcessor::createMapFile($file);
        } else {
            throw new RestException(500);
        }

        $result = UploadManager::getInstance()->getResult($collection, UploadManager::USER_FILE, intval($id));
        
        $conditionsMap = new ConditionsMap();
        $conditionsMap->setIncludeHidden(true);
        $conditionsMap->setController(get_class($this));
        //print_r(Application::getSQLLog());
        print_r(static::resultModefire($result->toArray(), $conditionsMap->getOptionsMap()));
        return static::resultModefire($result->toArray(), $conditionsMap->getOptionsMap());
    }

    /**
     * @header Access-Control-Allow-Origin: *
     * @param string $filter {@from query}
     * @param string $order {@from query}
     * @param string $fields {@from query}
     * @param int $page {@from query}
     * @param int $pagesize {@from query}
     * param string $Format {@from head}
     * smart-auto-routing false
     * @return array
     */
    public function index($filter = '', $order = '', $fields = '', $page = null, $pagesize = null)
    {
        $page = intval($page) > 0 ? intval($page) : 1;
        $pageSize = intval($pagesize) > 0 ? intval($pagesize) : 20;
    
        if(!class_exists($this->entity)) {
            throw new RestException(400, "'$this->entity' entity missing");
        }

        $query = $this->getQuery();
        $cm = static::createConditionsMap(get_class($this), $fields, $filter, $order);
    
        $result = static::getCollection($query, $cm, $page, $pageSize, true);
        array_walk($result['results'], function(&$item) {$item['type'] = 'f';});
        return $result;
    }


    /**
     * @param int $id {@from body}
     * smart-auto-routing false
     * @return array
     */
    public function delete($id)
    {
        $data = array('status' => 0);
        return static::modifier(get_class($this), $data,  (int)$id);
    }

    public function getConfig()
    {
        return [
            'entity' => 'App\Entity\UserFileGroupEntity',
            'primary' => 'id',
            'fields' => [
                'id' => [
                    'access' => BaseMapper::CONTROL_VISIBLE,
                 ],
                'name' => [
                    'relation' => BaseMapper::RELATION_BELONGSTO,
                    'validator'=>'String',
                    'resource' => 'v1\UserString',
                    'access' => BaseMapper::CONTROL_VISIBLE,
                    'inject' => 'text',
                    'foreignkey' => 'name_id'
                 ],
                'user' => [
                    'relation' => BaseMapper::RELATION_BELONGSTO,
                    'resource' => 'v1\User'
                 ],
                'status' => [
                    'validator'=>'Constrain',
                    'options' => ['values' => [
                        0 => 'Доступен',
                        1 => 'Недоступен'
                    ]],
                    'access' => BaseMapper::CONTROL_VISIBLE,
                    'modefire' => 'App\Modefire\Constrain'
                 ],
                'choice' => [
                    'validator'=>'Boolean',
                    'access' => BaseMapper::CONTROL_VISIBLE,
                 ],
                'admin_file' => [
                    'validator'=>'Boolean',
                 ],
                'files' => [
                    'relation' => BaseMapper::RELATION_HASMANY,
                    'access' => BaseMapper::CONTROL_VISIBLE,
                    'resource' => 'v1\UserFile',
                    'depth' => 2
                 ],
                'created_at' => [
                    'validator'=>'DateTime',
                    'modefire' => 'App\Model\Modefire\DateTime'
                 ],
                'updated_at' => [
                    'validator'=>'DateTime',
                    'modefire' => 'App\Model\Modefire\DateTime'
                 ],
            ],
        ];
    }
}

