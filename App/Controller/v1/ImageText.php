<?php
namespace v1;

use App\Application,
    App\Controller\AbstractController,
    App\Entity\LandmarkEntity,
    App\Model\Mapper\BaseMapper,
    App\Model\Utils\ImageUploadProcessor,
    App\Model\Mapper\ConditionsMap,
    App\Model\Manager\UploadManager
    //App\Model\v1\UserFileGroup
    ;

class ImageText extends AbstractController
{
    public function index($filter = '', $order = '', $fields = '', $page = null, $pagesize = null)
    {
        $data = 'Winter is come!';
        
        ini_set('max_execution_time', 0);
        
        $db = Application::getCapsule()->connection();
        
        if (!$db) {
            throw new RestException(500);
        }
        //$searchstring = 'www.cult-turist.ru/img/';
        $searchstring = '/upload/images/inText/';
        $x = $db->table('landmark')
                ->join('sitestring', 'landmark.full_text_id', '=', 'sitestring.id')
                ->where('sitestring.ru', 'LIKE', '%'.$searchstring.'%')
                ->select('landmark.id AS landmarkID','sitestring.id AS textID','sitestring.ru AS text')
                ->take(1700)
                ->skip(207)
                ->get();
                
        $pattern = '/\/upload\/images\/inText\/([^"]*)\"/';
        $resultMatch = array();
        $resultTest = array();
        $fullStack = array();
        
        
        $counter = 0;
        foreach($x as $key){
            preg_match_all($pattern, $key['text'], $matches,PREG_SET_ORDER);
            array_push($fullStack,array(
                                        'landmarkId' => $key['landmarkID'],
                                        'textId'     => $key['textID'],
                                        ));
            $fullStack[$counter]['files'] = array();
            
            for($j = 0; $j<count($matches); $j++){
                $tempPath = explode("/",$matches[$j][0]);
                $path = '/var/www/cult-turist.ru/www/public';
                for($i = 0; $i < count($tempPath)-1; $i++){
                    $path = $path.$tempPath[$i].'/';
                }
                $name = explode("/",$matches[$j][1]);
                array_push($fullStack[$counter]['files'],array(
                               'name' => $name[count($name)-1],
                               'path' => $path
                             ));
            }
            $fullStack[$counter]['files'] = array_map("unserialize", array_unique(array_map("serialize", $fullStack[$counter]['files'])));
            $counter++;
            
        }
        
        
        $prefire='';
        
        foreach($fullStack as $key)
        {
            $LandmarkObj = new Landmark();
            foreach($key['files'] as $way){
                
                $prefire = $this->indexationOfImage($way['path'],$way['name']);
                usleep(250000);
                $aData = $LandmarkObj ->get($key['landmarkId'],$filter = '', $order = '',$fields = 'gallery', $settings = '');
                $aData = $aData['gallery'];
                array_push($aData,$prefire);
                for ($i = 0; $i < count($aData); $i++) {
                	unset($aData[$i]['files']);
                }
                $resultInput = array(
                    'id'      => $key['landmarkId'],
                    'gallery' => $aData
                    );
                $LandmarkObj->put($key['landmarkId'],$resultInput);
            }
        }
        
        return ['data' => $resultInput,'preload' => $fullStack];
    }
    
    private function indexationOfImage($pathToFile, $fileName){
        
        
        
        $file = UploadManager::getInstance()->fileViaLocal($pathToFile, $fileName,UploadManager::USER_FILE);
        if(!$file) {
            
        }
        elseif($file->getOptions()['handling'] == 'image') {
            
            $collection = ImageUploadProcessor::createImagesBySizes($file, Application::getUserImageUploadSizes());
        }else {
            throw new RestException(500);
        }

        $result = UploadManager::getInstance()->getResult($collection, UploadManager::USER_FILE);

        //if ((int)$result->user->id === 1) {
            $result->status = 1;
            $result->save();
        //}
        $UserFileGroupObj = new UserFileGroup();
        $conditionsMap = new ConditionsMap();
        $conditionsMap->setIncludeHidden(true);
        $conditionsMap->setController(get_class($UserFileGroupObj));
        $temp = static::resultModefire($result->toArray(), $conditionsMap->getOptionsMap());
        return $UserFileGroupObj->get($temp['id'],$fields = 'files');
    }
    
}