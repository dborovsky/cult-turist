<?php
namespace v1;

use App\Controller\AbstractController,
    App\Entity\LandmarkEntity,
    App\Entity\AreaEntity,
    App\Application;

class SeeSquare extends AbstractController
{
    public function index($entity_name = '', $entity_id = '', $fields = '', $page = null, $pageSize = null)
    {
        $landmarkCm = static::createConditionsMap('v1\Landmark', 'id,name', 'landmark.id:eq:'.$entity_id);
        $curFeature = static::getSingle((new LandmarkEntity())->newQuery(), $landmarkCm, $entity_id);

        $nearestTmp = $this->_getNearestLandmark($curFeature['latitude'], $curFeature['longitude']);
        $nearest = [];
        foreach ($nearestTmp as $landmark) {
            if ((int)$landmark['id'] !== (int)$entity_id) {
                $nearest[$landmark['id']] = $landmark['id'];
            }
        }

        $landmarkCm2 = static::createConditionsMap('v1\Landmark', 'id,name,image.files,area,property.icon.files', 'id:in:'.implode(',', array_keys($nearest)));
        $data = static::getCollection((new LandmarkEntity())->newQuery(), $landmarkCm2, null, count($nearest));
        $data = array_reverse($data);
        /*foreach ($data as &$item) {
            $item['distance'] = round($nearest[$item['id']]*1000);
        }*/

        return ['data' => $data];
    }
    
    
    private function _getNearestLandmark($latitude, $longitude) {
        $db = Application::getCapsule()->connection();

        if (!$db) {
            throw new RestException(500);
        }
        
        return $db->table('landmark')->select("id")->whereBetween('latitude',[$latitude - 0.005, $latitude + 0.005])->whereBetween('longitude',[$longitude - 0.01, $longitude + 0.01])->get();
        /*return $db->select($db->raw("select id from landmark"))->whereBetween('latitude',[$latitude - 0.005, $latitude + 0.005])->whereBetween('longitude',[$longitude - 0.01, $longitude + 0.01]);*/
    }
}
?>