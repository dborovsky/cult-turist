<?php
namespace v1;

use App\Controller\AbstractController,
    App\Model\Utils\IUpload,
    Luracast\Restler\RestException,
    App\Model\Mapper\ConditionsMap,
    App\Model\Mapper\BaseMapper,
    App\Application;

class IblockBanner extends AbstractController
{
    
    /*public function index($entity_name = '', $entity_id = '', $fields = '', $page = null, $pageSize = null)
    {
        
        return ['data' => $data];
    }*/
    
    public function post($data = null){
        
        if(!class_exists($this->entity)) {
            throw new RestException(400, "'$this->entity' entity missing");
        }
        if(count($_FILES) < 1) {
            throw new RestException(400, "No files to upload");
        }
        //print_r($_FILES);
        foreach($_FILES as $alies=>$f) {
            $model = new IUpload();
            $result = $model->setResource($alies);
            break;
        }
        
        //print_r($result);
        
        $conditionsMap = new ConditionsMap();
        $conditionsMap->setIncludeHidden(true);
        $conditionsMap->setController(get_class($this));
        
        return static::resultModefire($result->toArray(), $conditionsMap->getOptionsMap());
    }
    
    public function put($id, $data = null)
    {
        if(intval($id) < 1) {
            throw new RestException(400, "Missing 'id' param!");
        }
        
        if(!class_exists($this->entity)) {
            throw new RestException(400, "'$this->entity' entity missing");
        }
        
        if(!isset($_FILES['AdvImage']) || !count($_FILES['AdvImage'])) {
            throw new RestException(400, "No files to upload");
        }
        
        $model = new IUpload();
        
        $file = $model->setResource($id);
        
        if(!$file) {
            throw new RestException(500, 'File type unsupported!');
        }
        
        $conditionsMap = new ConditionsMap();
        $conditionsMap->setIncludeHidden(true);
        $conditionsMap->setController(get_class($this));
        
        return static::resultModefire($file->toArray(), $conditionsMap->getOptionsMap());
        
        
    }
    
    private function _upload() {
        $status = 'Ok';
        return $status;
    }
    
    public function getConfig()
    {
        return [
            'entity' => 'App\Entity\IblockBannerEntity',
            'primary' => 'id',
            'fields' => [
                'id' => [
                    'access' => BaseMapper::CONTROL_VISIBLE,
                 ],
                'path' => [
                    'validator'=>'String',
                    'access' => BaseMapper::CONTROL_VISIBLE,
                 ],
                'width' => [
                    'access' => BaseMapper::CONTROL_VISIBLE,
                 ],
                'height' => [
                    'access' => BaseMapper::CONTROL_VISIBLE,
                 ],
                'created_at' => [
                    'validator'=>'DateTime',
                    'modefire' => 'App\Model\Modefire\DateTime'
                 ],
                'updated_at' => [
                    'validator'=>'DateTime',
                    'modefire' => 'App\Model\Modefire\DateTime'
                 ],
            ],
        ];
    }
}
?>