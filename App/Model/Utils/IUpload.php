<?php

namespace App\Model\Utils;

use App\Application,
App\Entity\IblockBannerEntity;

class IUpload {
    
    private $db;
    
    public function __construct(){
        
        $this->db = Application::getCapsule()->connection();
        
    }
    
    public function setResource($alies,$id = 0){
        
        $files = $_FILES[$alies];
        $file = $files['tmp_name'];
        if (is_uploaded_file($file)) {
            list($filename, $fileextention) = explode('.', $files['name']);
            $newName = md5($filename.'_'.time()).'.'.$fileextention;
            move_uploaded_file($file,'upload/advert/'.$newName);
            $size = getimagesize('upload/advert/'.$newName);
            $width = $size[0];
            $height = $size[1];
        }
        
        
        if($id > 0) {
            $entity = IblockBannerEntity::with()->find($id);
            print_r($entity);
            if(!$entity) {
                throw new RestException(400, "File with id = $id not found!");
            }
        } else {
            $entity = new IblockBannerEntity();
        }
        
        //$filesToSave = array();
        
        /*$filesToSave = new IblockBannerEntity([
                            'path' => 'upload/advert/'.$newName,
                            'width' => $width,
                            'height' => $height
                        ]);*/
        
        $entity->path   = 'upload/advert/'.$newName;
        $entity->width  = $width;
        $entity->height = $height;
        
        $entity->save();
        
        return $entity;
    }
    
    
}
?>