<?php

namespace App\Entity;

class PeopleEntity extends AbstractEntity {

    protected $table = 'people';

    // unified alias
    public function parent_area()
    {
        return $this->area();
    }

    public function name()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'name_id');
    }
    
    public function original_name()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'original_name_id');
    }
    
    public function detail()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'detail_id');
    }
    
    public function image()
    {
        return $this->belongsTo('App\Entity\FileGroupEntity', 'image_id');
    }

    public function web_image()
    {
        return $this->belongsTo('App\Entity\FileGroupEntity', 'web_image_id');
    }
    
    public function area()
    {
        return $this->belongsToMany('App\Entity\AreaEntity', 'people_area', 'people_id', 'area_id')->withPivot('system')->withTimestamps();
    }
   
    public function prregion()
    {
        return $this->belongsToMany('App\Entity\AreaEntity', 'people_area', 'people_id', 'region_id')->withPivot('system')->withTimestamps();
    }
    
    public function prcountry()
    {
        return $this->belongsToMany('App\Entity\AreaEntity', 'people_area', 'people_id', 'country_id')->withPivot('system')->withTimestamps();
    }
   
    public function comment()
    {
        return $this->belongsToMany('App\Entity\CommentEntity', 'people_comment', 'people_id', 'comment_id')->withTimestamps();
    }
    
    public function like()
    {
        return $this->belongsToMany('App\Entity\LikeEntity', 'people_like', 'people_id', 'like_id')->withTimestamps();
    }

    public function seealso()
    {
        return $this->belongsToMany('App\Entity\FeatureEntity', 'feature_seealso', 'feature_id', 'seealso_id')->withTimestamps();
    }
    
}
