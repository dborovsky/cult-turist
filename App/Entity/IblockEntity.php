<?php
namespace App\Entity;
    
class IblockEntity extends AbstractEntity {
    
    protected $table = 'iblocktemp';
    
    public function name()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'name_id');
    }
    
    public function area()
    {
        return $this->belongsToMany('App\Entity\AreaEntity', 'iblock_area', 'iblock_id', 'area_id')->withTimestamps();
    }
    
    public function partner_name()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'partner_id');
    }
    
     public function banner_link()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'link_id');
    }
    
    public function banner()
    {
        return $this->belongsTo('App\Entity\IblockBannerEntity', 'banner_id');
    }
    
    public function widget()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'widget_id');
    }
    
    /*public function pages()
    {
        return $this->belongsTo('App\Entity\IblockPagesEntity', 'pages_id');
    }*/
    
    /*public function a()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'widget_id');
    }*/
}
?>