<?php

namespace App\Entity;

class KitchenEntity extends AbstractEntity {

    protected $table = 'kitchen';
    
    // unified alias
    public function parent_area()
    {
        return $this->area();
    }

    public function name()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'name_id');
    }
    
    public function original_name()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'original_name_id');
    }
    
    public function text()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'text_id');
    }
    
    public function image()
    {
        return $this->belongsTo('App\Entity\FileGroupEntity', 'image_id');
    }

    public function web_image()
    {
        return $this->belongsTo('App\Entity\FileGroupEntity', 'web_image_id');
    }
    
    public function area()
    {
        return $this->belongsToMany('App\Entity\AreaEntity', 'kitchen_area', 'kitchen_id', 'area_id')->withPivot('system')->withTimestamps();
    }
    
    public function prregion()
    {
        return $this->belongsToMany('App\Entity\AreaEntity', 'kitchen_area', 'kitchen_id', 'region_id')->withPivot('system')->withTimestamps();
    }
    
    public function prcountry()
    {
        return $this->belongsToMany('App\Entity\AreaEntity', 'kitchen_area', 'kitchen_id', 'country_id')->withPivot('system')->withTimestamps();
    }
    
    public function iblock()
    {
        return $this->belongsToMany('App\Entity\IblockEntity', 'iblock_kitchen', 'kitchen_id', 'iblock_id')->withTimestamps();
    }
    
    public function comment()
    {
        return $this->belongsToMany('App\Entity\CommentEntity', 'kitchen_comment', 'kitchen_id', 'comment_id')->withTimestamps();
    }
    
    public function like()
    {
        return $this->belongsToMany('App\Entity\LikeEntity', 'kitchen_like', 'kitchen_id', 'like_id')->withTimestamps();
    }

    public function seealso()
    {
        return $this->belongsToMany('App\Entity\FeatureEntity', 'feature_seealso', 'feature_id', 'seealso_id')->withTimestamps();
    }
    
}
