<?php
namespace App\Entity;
    
class IblockBannerEntity extends AbstractEntity {
    protected $table = 'iblock_banner';
    
    /*public function iblock_id()
    {
        return $this->belongsTo('App\Entity\IblockEntity', 'iblock_id');
    }*/
    
    protected $fillable = [
        'path',
        'width',
        'height'
    ];
}
?>