<?php

namespace App\Entity;

use App\Entity\Mixins\HasAreaLink;

class EventEntity extends AbstractEntity {

    use HasAreaLink;

    protected $table = 'event';
    
    public function name()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'name_id');
    }
    
    public function original_name()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'original_name_id');
    }
    
    public function text()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'text_id');
    }

    public function date()
    {
        return $this->belongsTo('App\Entity\SiteStringEntity', 'date_id');
    }

    public function image()
    {
        return $this->belongsTo('App\Entity\FileGroupEntity', 'image_id');
    }

    public function web_image()
    {
        return $this->belongsTo('App\Entity\FileGroupEntity', 'web_image_id');
    }

//    public function area()
//    {
//        return $this->belongsToMany('App\Entity\AreaEntity', 'event_area', 'event_id', 'area_id')->withPivot('system')->withTimestamps();
//    }
    
    public function iblock()
    {
        return $this->belongsToMany('App\Entity\IblockEntity', 'iblock_event', 'event_id', 'iblock_id')->withTimestamps();
    }
    
    public function comment()
    {
        return $this->belongsToMany('App\Entity\CommentEntity', 'event_comment', 'event_id', 'comment_id')->withTimestamps();
    }
    
    public function like()
    {
        return $this->belongsToMany('App\Entity\LikeEntity', 'event_like', 'event_id', 'like_id')->withTimestamps();
    }

    public function seealso()
    {
        return $this->belongsToMany('App\Entity\EventEntity', 'event_seealso', 'event_id', 'seealso_id')->withTimestamps();
    }
}

?>
