<?php
/**
 * Created by PhpStorm.
 * User: karellen
 * Date: 6/8/16
 * Time: 11:30 PM
 */

namespace App\Entity\Mixins;


trait HasAreaLink
{
    private $entity;

    public function area()
    {
        $entity = $this->getEntity();
        if($entity == 'Area') {
            return $this->belongsToMany('App\Entity\AreaEntity', 'arealinks', 'child_id', 'parent_id')->withPivot('capital')->withTimestamps();
        } else {
            $pivot = strtolower($entity).'_area';
            $foreign_key = strtolower($entity).'_id';
            return $this->belongsToMany('App\Entity\AreaEntity', $pivot, $foreign_key, 'area_id')->withPivot('system')->withTimestamps();
        }
    }

    // deprecated
    public function parent_area()
    {
        return $this->area();
    }

    public function prregion()
    {
        $entity = $this->getEntity();
        $pivot = strtolower($entity).'_area';
        $foreign_key = strtolower($entity).'_id';
        return $this->belongsToMany('App\Entity\AreaEntity', $pivot, $foreign_key, 'region_id')->withPivot('system')->withTimestamps();
    }

    public function prcountry()
    {
        $entity = $this->getEntity();
        $pivot = strtolower($entity).'_area';
        $foreign_key = strtolower($entity).'_id';
        return $this->belongsToMany('App\Entity\AreaEntity', $pivot, $foreign_key, 'country_id')->withPivot('system')->withTimestamps();
    }

    private function getEntity()
    {
        $pure_name = substr(get_class(),(int)strrpos(get_class(), '\\')+1);
        return substr($pure_name,0,strpos($pure_name,'Entity'));
    }
}