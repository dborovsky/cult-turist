
CREATE TABLE feature_seealso (
  feature_id  int(10) UNSIGNED NOT NULL,
  seealso_id  int(10) UNSIGNED NOT NULL,
  created_at  timestamp NULL,
  updated_at  timestamp NULL,
  /* Keys */
  PRIMARY KEY (feature_id, seealso_id),
  /* Foreign keys */
  CONSTRAINT fe_feature_seealso2
    FOREIGN KEY (seealso_id)
    REFERENCES feature(id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION, 
  CONSTRAINT fk_feature_seealso1
    FOREIGN KEY (feature_id)
    REFERENCES feature(id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = InnoDB;