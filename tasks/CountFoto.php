<?php

namespace App;

require 'DBConn.php';

use Illuminate\Database\Capsule\Manager as Capsule;

class CountFoto extends DBConn
{
    private function getEntityId($entity_name){
        $result = Capsule::table($entity_name)
            ->lists('id');

        return $result;
    }
    
    private function fotoCounter(){
        
        $withGal = ['area','landmark'];
        
        foreach ($withGal as $value) {
            
            $entitis = $this->getEntityId($value);
            
            foreach ($entitis as $id){
                
                $result = Capsule::table($value)
                ->join($value.'_gallery',$value.'_id','=',$value.'.id')
                ->join('userfilegroup',$value.'_gallery.user_file_id','=','userfilegroup.id')
                ->where($value.'_id',$id)
                ->where('userfilegroup.status',1)
                ->count();
        
                $mainFoto = Capsule::table($value)
                ->join('filegroup',$value.'.image_id','=','filegroup.id')
                ->where($value.'.id',$id)
                ->where('filegroup.id','>',1)
                ->whereNotIn('filegroup.id',[1,2,10,11,12,13])
                ->count();
                
                $fotoCount = $result+$mainFoto;
                
                Capsule::table($value)
                ->where('id', $id)
                ->update(['foto_count' => $fotoCount]);
            }    
            
        }
        
        $withoutGal = ['people','feature','kitchen'];
        
        foreach ($withoutGal as $value) {
            
            $entitis = $this->getEntityId($value);
            
            foreach ($entitis as $id){
        
                $mainFoto = Capsule::table($value)
                ->join('filegroup',$value.'.web_image_id','=','filegroup.id')
                ->where($value.'.id',$id)
                ->where('filegroup.id','>',1)
                ->whereNotIn('filegroup.id',[1,2,10,11,12,13])
                ->count();
                
                $fotoCount = $mainFoto;
                
                Capsule::table($value)
                ->where('id', $id)
                ->update(['foto_count' => $fotoCount]);
            
            }        
        }
        
    }
    
        
    public function runCountFoto(){
        $this->fotoCounter();
    }
    
}

$binder = new CountFoto();

$binder->runCountFoto();