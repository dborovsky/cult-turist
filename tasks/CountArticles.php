<?php

namespace App;

require 'DBConn.php';

use Illuminate\Database\Capsule\Manager as Capsule;

class CountArticles extends DBConn
{
    const STATUS_ACTIVE = '1';

    const VICINITY = 'vicinity';

    const LANDMARK = 'landmark';

    private $sourceTableName;

    private $articleCountFieldName;

    private $targetTableName;

    private $entityName;

    private $isVicinity = false;

    private function setTargetTableName($table_name)
    {
        $this->targetTableName = $table_name;
    }

    private function setSourceTableName($table_name)
    {
        $this->sourceTableName = $table_name . '_area';
    }

    private function setArticleCountFieldName($field_name)
    {
        if ($this->isVicinity)
        {
            $field_name = self::VICINITY;
        }

        $this->articleCountFieldName = $field_name . '_count';
    }

    private function setEntityName($entity_name)
    {
        $this->entityName = $entity_name;
    }

    private function setEntity($entity_name)
    {
        $this->setTargetTableName('area');
        $this->setSourceTableName($entity_name);
        $this->setEntityName($entity_name);
        $this->setArticleCountFieldName($entity_name);
    }

    private function getAreasId()
    {
        $result = Capsule::table($this->targetTableName)
            ->lists('id');

        return $result;
    }
    
    private function getArticleCount($id)
    {
        if ($this->entityName == self::LANDMARK)
        {
            $result = Capsule::table($this->sourceTableName)
                ->leftJoin($this->entityName, $this->sourceTableName.'.'.$this->entityName.'_id', '=', $this->entityName.'.id')
                ->where($this->sourceTableName.'.area_id', '=', $id)
                ->where($this->sourceTableName.'.'.self::VICINITY, '=', (int)$this->isVicinity)
                ->where($this->entityName.'.status', '=', self::STATUS_ACTIVE)
                ->count();
        }
        else
        {
            $result = Capsule::table($this->sourceTableName)
                ->leftJoin($this->entityName, $this->sourceTableName.'.'.$this->entityName.'_id', '=', $this->entityName.'.id')
                ->where($this->sourceTableName.'.area_id', '=', $id)
                ->where($this->entityName.'.status', '=', self::STATUS_ACTIVE)
                ->count();
        }

        return $result;
    }

    private function setArticleCount($id, $count)
    {
        Capsule::table($this->targetTableName)
            ->where('id', '=', $id)
            ->update([
                $this->articleCountFieldName => $count
            ]);
    }

    private function countArticles()
    {
        $areas = $this->getAreasId();

        foreach ($areas as $area_id)
        {
            $article_count = $this->getArticleCount($area_id);

            $this->setArticleCount($area_id, $article_count);
        }
    }
    
    public function run($article)
    {
        if ($article == self::VICINITY)
        {
            $article = self::LANDMARK;
            $this->isVicinity = true;
        }
 
 
        $this->setEntity($article);
        $this->countArticles();
        
    }

}

$articles_to_count = [
    'kitchen',
    'feature',
    'people',
    'landmark',
    'event',
    'vicinity'
];

$binder = new CountArticles();

foreach ($articles_to_count as $entity)
{
    $binder->run($entity);
}
