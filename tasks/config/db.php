<?php

return [
    'db_local_test' => [
        'driver'    => 'mysql',
        'username'  => 'root',
        'host'      => 'localhost',
        'password'  => '',
        'database'  => 'cultturist_test',
        'charset'   => 'utf8',
    ],
    'db_local' => [
        'driver'    => 'mysql',
        'username'  => 'root',
        'host'      => 'localhost',
        'password'  => '',
        'database'  => 'cultturist',
        'charset'   => 'utf8',
    ],
    'db_prod' => [
        'driver'    => 'mysql',
        'username'  => 'cultturist',
        'host'      => 'localhost',
        'password'  => 'zse4xsw2CDE^',
        'database'  => 'cultturist',
        'charset'   => 'utf8',
    ],
    'db_cloud' => [
        'driver'    => 'mysql',
        'username'  => getenv('C9_USER'),
        'host'      => getenv('IP'),
        'password'  => '',
        'database'  => 'c9',
        'charset'   => 'utf8',
    ],
];
