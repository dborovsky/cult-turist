<?php

namespace App;

require 'DBConn.php';

use Illuminate\Database\Capsule\Manager as Capsule;

class BindEntities extends DBConn
{
    const DATETIME_FORMAT           = 'Y-m-d H:i:s';
    const AREA_LINKS_TABLE_NAME     = 'arealinks';
    const PARENT_ID_FIELD           = 'parent_id';
    const CHILD_ID_FIELD            = 'child_id';
    const AREA_ID_FIELD             = 'area_id';

    private $targetTableName;

    private $entityIdFieldName;

    private function setTargetTableName($table_name)
    {
        $this->targetTableName = $table_name . '_area';
    }

    private function setEntityIdFieldName($field_name)
    {
        $this->entityIdFieldName = $field_name . '_id';
    }

    private function setEntity($entity_name)
    {
        $this->setTargetTableName($entity_name);
        $this->setEntityIdFieldName($entity_name);
    }

    private function getEntityAreaLinks()
    {
        $entity = Capsule::table($this->targetTableName)
            ->get();

        return $entity;
    }

    private function getParentLinksOfArea($area_id)
    {
        $links = Capsule::table(self::AREA_LINKS_TABLE_NAME)
            ->where(self::CHILD_ID_FIELD, '=', $area_id)
            ->get();

        return $links;
    }

    private function getChildLinksOfArea($area_id)
    {
        $links = Capsule::table(self::AREA_LINKS_TABLE_NAME)
            ->where(self::PARENT_ID_FIELD, '=', $area_id)
            ->get();

        return $links;
    }

    private function getAreaType($area_id)
    {
        $area_type = Capsule::table('area')
            ->where('id', '=', $area_id)
            ->first([
                'area_type'
            ]);

        return $area_type['area_type'];
    }

    /**
     * @param $links array of links
     */
    private function insertNewEntityLinks($links)
    {
        foreach ($links as $link) {
            Capsule::table($this->targetTableName)
                ->insert($link);
        }
    }

    private function isLinkExist($new_link, $links)
    {
        foreach ($links as $link)
        {
            $a = $new_link[self::AREA_ID_FIELD] == $link[self::AREA_ID_FIELD];
            $b = $new_link[$this->entityIdFieldName] == $link[$this->entityIdFieldName];

            if ($a && $b)
            {
                return true; // нашли дубликат, выходим
            }
        }

        return false;
    }

    private function bindEntitiesToAreas()
    {

        $entity_area_links = $this->getEntityAreaLinks();

        $i = 0;

        $all_new_entity_links = []; // чтобы не делать лишние запросы

        foreach ($entity_area_links as $ea_link) {
            $now = date(self::DATETIME_FORMAT);

            $new_entity_area_links = []; // новые привязки сущностей, которые будем генерировать

//            $parentAreaLinks    = $this->getParentLinksOfArea($ea_link[self::AREA_ID_FIELD]);
            $childAreaLinks     = $this->getChildLinksOfArea($ea_link[self::AREA_ID_FIELD]);
            $areaType           = $this->getAreaType($ea_link[self::AREA_ID_FIELD]);

            if ($areaType == '0' || $areaType == '1') {
                $i++;
                foreach ($childAreaLinks as $child_link) {
                    $new_entity_area_link = [
                        self::AREA_ID_FIELD      => $child_link[self::CHILD_ID_FIELD],
                        $this->entityIdFieldName => $ea_link[$this->entityIdFieldName],
                        'system'                 => 1,
                        'created_at'             => $now,
                        'updated_at'             => $now
                    ];

                    if (!$this->isLinkExist($new_entity_area_link, $entity_area_links) &&
                        !$this->isLinkExist($new_entity_area_link, $all_new_entity_links) &&
                        !$this->isLinkExist($new_entity_area_link, $new_entity_area_links))
                    {
                        $new_entity_area_links[] = $new_entity_area_link;

                        $all_new_entity_links[] = $new_entity_area_link; // храним все привязки текущего запуска скрипта
                    }
                }

//                echo $ea_link[$this->entityIdFieldName] . ' ';

                $this->insertNewEntityLinks($new_entity_area_links);
            }
        }

//        echo PHP_EOL;
//        echo 'K: ' . $i;
    }

    public function run($entity_name)
    {
        $this->setEntity($entity_name);
        $this->bindEntitiesToAreas();
    }
}

$entites_to_bind = [
    'kitchen',
    'feature',
    'people'
//    'event' // события не перенесены - 01.06.2015
];

////$binder->run($_SERVER['argv'][1]);

$binder = new BindEntities();
foreach ($entites_to_bind as $entity)
{
    $binder->run($entity);
}