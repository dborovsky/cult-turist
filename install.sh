sudo apt-get update

# Install and setting up Apache
sudo apt-get install -y apache2
# Remove /var/www path
rm -rf /var/www
# Symbolic link to /vagrant/site path
ln -fs /vagrant /var/www
# Enable mod_rewrite
a2enmod rewrite

# Installing MySQL
export DEBIAN_FRONTEND=noninteractive
apt-get install -y mysql-server

# Installing handy packages
apt-get install -y curl unzip imagemagick npm nodejs-legacy git

# Creating the database 'cultturist'
mysql -u root -e "create database cultturist character set utf8 collate utf8_unicode_ci;"
mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '' WITH GRANT OPTION; FLUSH PRIVILEGES;"

# Installing PHP Modules
apt-get install -y php5 php5-cli php5-gd php5-curl php5-mysql php5-mcrypt php5-imagick php5-xdebug libapache2-mod-auth-mysql libapache2-mod-php5

sed -i 's/\/var\/www\/html/\/var\/www/g' /etc/apache2/sites-available/000-default.conf

sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 20M/g' /etc/php5/apache2/php.ini
echo "xdebug.remote_enable = on" >> /etc/php5/apache2/php.ini
echo "xdebug.remote_connect_back = on" >> /etc/php5/apache2/php.ini
echo "xdebug.idekey = \"vagrant\"" >> /etc/php5/apache2/php.ini

#<Directory /var/www/>
#        Options Indexes FollowSymLinks
#        AllowOverride All
#        Require all granted
#</Directory>

# Restarting Apache
service apache2 restart

# Installing Bower Components
npm install -g bower

# populate database yourself
# Add Restler to vendor manually
# cd /vagrant/public/site
# bower install